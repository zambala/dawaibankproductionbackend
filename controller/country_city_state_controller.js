var express = require('express');
var router = express.Router();
var db = require('./dbconnections_controller');
var util = require('./../config/helper');
 

//::LIST for admin to activate / inactivate 
router.post('/:id', function (req, res) {

    var params = req.body; 

    var id = req.params.id

    var activatecountry = util.IsNull(params.activatecountry);
    var activatestate = util.IsNull(params.activatestate);
    var activatecity = util.IsNull(params.activatecity); 
    var action = util.IsNull(params.action, false);
     var sql ='';

    if (activatecountry == 1)
          sql = "update countrylist set status=$2 where countryid=$1";
    if (activatestate == 1)
          sql = "update statelist set status=$2 where id=$1";
    if (activatecity == 1)
          sql = "update citylist set status=$2 where city_id=$1";
      
     console.log(' updatecountrystatecity ',sql,id,action);   

    db.any(sql, [id, action])
        .then(function (results) {
            res.status(200);
            res.send(results);
        })
        .catch(function (err) {
            console.log('Error Country State City Update ',err);
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });

});

 
module.exports = router;