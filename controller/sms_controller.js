var express = require('express');
var router = express.Router();
var db = require('./dbconnections_controller');
var util = require('./../config/helper');
var request = require("request");
var utilCtrl = require('./utility');

//::LIST
router.get('/smstovolunteer', function (req, res) {



    var sql_ticket = "select city_id from ticket where ticketstatus=1 group by city_id ";
    db.any(sql_ticket)
        .then(function (results_ticket) {

            if (results_ticket && results_ticket.length > 0) {
                cityid = results_ticket.map(function (re) {
                    // console.log(re.city_id);
                    return re.city_id
                });
                console.log('sms cityid ::', cityid);
                var sql_voluntier = "SELECT mobileno,email,fullname FROM users WHERE userrole=3 AND userstatus='1' AND mobileno is not null AND (address::json->>'city')::integer = any($1)";
                db.any(sql_voluntier, [cityid]).then(function (resultvoultier) {
                    console.log(resultvoultier);
                    var mobilenumber = resultvoultier.map(function (mo) {
                          console.log(mo.mobileno);
                          sendMailToVolunteer(mo.email,mo.fullname);
                        return mo.mobileno;
                    }).toString();
                    var message = 'Hey Volunteer! There are open tickets in your area, please go ahead and assign:)';

             /*       utilCtrl.sendSMS(message, mobilenumber, function (success) {
                        if (success) {
                            res.status(200);
                            res.send({
                                status: 200,
                                message: "successfully send SMS"
                            });
                        } else {
                            res.status(400);
                            res.send({
                                status: 400,
                                message: "SMS send failed"
                            });
                        }
                    }); */

                }).catch(function (err) {
                    var code = err.code == 23505 ? 409 : 400
                    res.status(code);
                    res.send(err);
                })


            }
        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });

});


function sendMailToVolunteer(argEmailid,argFullName){
  console.log('sendMailToVolunteer',argEmailid,' ::',argFullName);
 
}


function scheduleNotificationToVolunteer() {
    var sql_ticket = "select city_id from ticket where ticketstatus=1 group by city_id ";
    db.any(sql_ticket)
        .then(function (results_ticket) {

            if (results_ticket && results_ticket.length > 0) {
                cityid = results_ticket.map(function (re) {
                    // console.log(re.city_id);
                    return re.city_id
                });
                console.log('sms cityid ::', cityid);
                var sql_voluntier = "SELECT mobileno FROM users WHERE userrole=3 AND userstatus='1' AND mobileno is not null AND (address::json->>'city')::integer = any($1)";
                db.any(sql_voluntier, [cityid]).then(function (resultvoultier) {
                    console.log(resultvoultier);
                    var mobilenumber = resultvoultier.map(function (mo) {
                        //  console.log(mo.mobileno);
                        return mo.mobileno;
                    }).toString();
                    var message = 'Hey Volunteer! There are open tickets in your area, please go ahead and assign:)';

                    utilCtrl.sendSMS(message, mobilenumber, function (success) {
                        if (success) {
                            res.status(200);
                            res.send({
                                status: 200,
                                message: "successfully send SMS"
                            });
                        } else {
                            res.status(400);
                            res.send({
                                status: 400,
                                message: "SMS send failed"
                            });
                        }
                    });

                }).catch(function (err) {
                    var code = err.code == 23505 ? 409 : 400
                    res.status(code);
                    res.send(err);
                })


            }
        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });
}

var schedule = require('node-schedule');

function scheduleNotification() {
    schedule.scheduleJob('12 30 00 * * *', scheduleNotificationToVolunteer);
}



//module.exports = router;

module.exports = {scheduleNotification:scheduleNotification};