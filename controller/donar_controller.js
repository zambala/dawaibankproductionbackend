var express = require('express');
var router = express.Router();
var db = require('./dbconnections_controller');
var util = require('./../config/helper');
var utilCtrl = require('./utility');

const request = require('request');

//::LIST
router.get('/', function (req, res) {

    //console.log('DONAR :: ',req.protocol, req.headers.host);
    /* const options = {  
        url:req.protocol+'://'+req.headers.host+'/api/user/',
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Accept-Charset': 'utf-8',
            'User-Agent': 'my-reddit-client'
        }
    };
    
    request(options, function(err, res, body) {  
        let json = JSON.parse(body);
        console.log(json);
    }); */

});

router.post('/', function (req, resultDonar) {
    
   req.body.allownotoken=1;
    var donateProductType = req.body.producttype;
    var useroptions = {
        url: req.protocol + '://' + req.headers.host + '/api/user/',
        method: 'POST',
        body: req.body,
        json: true,
        headers: {
            'Accept': 'application/json',
            'Accept-Charset': 'utf-8',
            'User-Agent': 'my-reddit-client'
        }
    };

    var  ticketoptions = {
        url: req.protocol + '://' + req.headers.host + '/api/ticket/',
        method: 'POST',
        body:req.body,
        json: true,
        headers: {
            'Accept': 'application/json',
            'Accept-Charset': 'utf-8',
            'User-Agent': 'my-reddit-client'
        }
    };

    var medicineoptions = {
        url: req.protocol + '://' + req.headers.host + '/api/medicine/insertmultiple/medicine/',
        method: 'POST',
        body: req.body,
        json: true,
        headers: {
            'Accept': 'application/json',
            'Accept-Charset': 'utf-8',
            'User-Agent': 'my-reddit-client'
        }
    };

    var otherProductoptions = {
        url: req.protocol + '://' + req.headers.host + '/api/medicine/add/otherproduct/',
        method: 'POST',
        body: req.body,
        json: true,
        headers: {
            'Accept': 'application/json',
            'Accept-Charset': 'utf-8',
            'User-Agent': 'my-reddit-client'
        }
    };

    var emailoptions = {
        url: req.protocol + '://' + req.headers.host + '/api/sendmail/email',
        method: 'POST',
        body: '',
        json: true,
        headers: {
            'Accept': 'application/json',
            'Accept-Charset': 'utf-8',
            'User-Agent': 'my-reddit-client'
        }
    };

    var emailJsonObj = {
        'email': req.body.email,
        'mailtype':'newuser'
    };
    emailoptions.body = emailJsonObj;

    request(useroptions, function (err, res, body) {
        let userid = '',
            ticketid = '';


        if (body.userid != '' && body.userid != null) {
            userid = body.userid;
            var jsonDataObj = {
                'userid': userid,
                'addedby': userid,
                'centerid':req.body.centerid,
                'cityid':req.body.address.city,
                'donarcomment':req.body.donarcomment,
                'timeofcontact':req.body.timeofcontact,
                'allownotoken':1
            };
            ticketoptions.body = jsonDataObj;
            request(ticketoptions, function (err, res, body) {
                if (body.message == 'Created Successfully' && body.id != '') {
                    ticketid = body.id;
                    medicineoptions.body['userid'] = userid;
                    medicineoptions.body['ticketid'] = ticketid;
                    medicineoptions.body['addedby'] = userid;
                    medicineoptions.body['allownotoken'] =1;
                      callMethod ='';
                    if(donateProductType ==2){
                        callMethod = otherProductoptions;
                     }
                     else{
                        callMethod = medicineoptions;
                     }
                    console.log('Donar Controller CallMethod',callMethod,' :donateProductType: ',donateProductType);
                    request(callMethod, function (err, res, body) {
                        console.log('Medicine Response ::', body);
                        if (body.message == 'created successfully' && body.id != '') {
                            var emailObj = {
                                name:req.body.fullname,
                                email: req.body.email,
                                type: 'newdonar',
                                ticketid:ticketid
                            }
                            utilCtrl.sendEmail(emailObj, function (success) {
                                console.log('mail delivered from add user: ' + success);
                            });
                
                            resultDonar.send({
                                message: "Successfully added",
                                ticketid: ticketid,
                                id: userid
                            });

                        } else {
                            resultDonar.status(401);
                            resultDonar.send({
                                message: "Error in donate medicine"
                            });


                        }
                    });
                } else {
                    console.log('Donar Ticker Creation ERROR ',err);
                }

            });
        } else {
            console.log('Donar Creation ERROR', err, res);
        }

    });

});

module.exports = router;