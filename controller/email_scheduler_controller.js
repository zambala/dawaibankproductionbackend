var express = require('express');
var router = express.Router();
var db = require('./dbconnections_controller');
var util = require('./../config/helper');
var request = require("request");
var utilCtrl = require('./utility');

//::LIST
router.get('/emailtovolunteer', function (req, res) {
    var sql_ticket = "select city_id from ticket where ticketstatus=1 group by city_id ";
    db.any(sql_ticket)
        .then(function (results_ticket) {

            if (results_ticket && results_ticket.length > 0) {
                cityid = results_ticket.map(function (re) {
                    // console.log(re.city_id);
                    return re.city_id
                });
                console.log('sms cityid ::', cityid);
                var sql_voluntier = "SELECT mobileno,email,fullname FROM users WHERE userrole=3 AND userstatus='1' AND mobileno is not null AND (address::json->>'city')::integer = any($1)";
                db.any(sql_voluntier, [cityid]).then(function (resultvoultier) {
                    console.log(resultvoultier);
                    var mobilenumber = resultvoultier.map(function (mo) {
                          console.log(mo.mobileno);
                         // sendMailToVolunteer(mo.email,mo.fullname);
                       // return mo.mobileno;
                       var emailObj = { 
                        name: mo.fullname,
                        email: mo.email,
                        type: 'email-scheduler-volunteer'
                    }
                    utilCtrl.sendEmail(emailObj, function (success) {
                        console.log('mail delivered approve user request: ' + success);
                    });



                    }) 
                    

                }).catch(function (err) {
                    var code = err.code == 23505 ? 409 : 400
                    res.status(code);
                    res.send(err);
                })


            }
        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        }); 
});

function scheduleNotificationToVolunteerByEmail() {


    var sql_ticket = "select city_id from ticket where ticketstatus=1 group by city_id ";
    db.any(sql_ticket)
        .then(function (results_ticket) {

            if (results_ticket && results_ticket.length > 0) {
                cityid = results_ticket.map(function (re) {
                    // console.log(re.city_id);
                    return re.city_id
                });
                console.log('sms cityid ::', cityid);
                var sql_voluntier = "SELECT mobileno,email,fullname FROM users WHERE userrole=3 AND userstatus='1' AND mobileno is not null AND (address::json->>'city')::integer = any($1)";
                db.any(sql_voluntier, [cityid]).then(function (resultvoultier) {
                    console.log(resultvoultier);
                    var mobilenumber = resultvoultier.map(function (mo) {
                          console.log(mo.mobileno);
                         // sendMailToVolunteer(mo.email,mo.fullname);
                       // return mo.mobileno;
                       var emailObj = { 
                        name: mo.fullname,
                        email: mo.email,
                        type: 'email-scheduler-volunteer'
                    }
                    utilCtrl.sendEmail(emailObj, function (success) {
                        console.log('mail delivered approve user request: ' + success);
                    });



                    }) 
                    

                }).catch(function (err) {
                    var code = err.code == 23505 ? 409 : 400
                    res.status(code);
                    res.send(err);
                })


            }
        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        }); 


}
 
var schedule = require('node-schedule');

function scheduleNotificationEmail() {
    schedule.scheduleJob('12 30 00 * * *', scheduleNotificationToVolunteerByEmail);
}



//module.exports = router;

module.exports = {scheduleNotificationEmail:scheduleNotificationEmail};

 
 