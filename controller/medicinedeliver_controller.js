var express = require('express');
var router = express.Router();
var db = require('./dbconnections_controller');
var util = require('./../config/helper');

 

//::ADD
router.post('/', function (req, res) {
    var params = req.body;

    var deliverby = util.IsNull(params.userid);
    var centerid = util.IsNull(params.centerid);
    
    var cityid = util.IsNull(params.cityid);
    var patientId = util.IsNull(params.patientId);
    var medicine = util.IsNull(params.medicineInfo); 
    var patientreferenceinfo =   util.IsNull(params.patientreferenceinfo); 
    var medicineValue  ='';
    for (var i = 0; i < medicine.length; i++) {
 
        var medicineid = util.IsNull(medicine[i].medicineid);
        var develiverQty = util.IsNull(medicine[i].develiverQty);         
        medicineValue   = medicineValue +"(nextval('medicine_log_seq'),"+medicineid+","+ develiverQty+",'"+ patientId+"',"+ deliverby+",now(),"+ centerid+","+cityid+"),";
        updateMedicineAvailableQuantity(deliverby,develiverQty,medicineid)
    }
      medicineValue = medicineValue.substring(0, medicineValue.length-1);  
    var sql = "INSERT INTO medicinelog(medicinelogid,medicineid, quantity, receiverid, dispatchedby, dispatchedon, centerid,cityid)"
    sql += " VALUES " + medicineValue;
 // console.log('deliver medicine', sql);
     db.any(sql)
        .then(function (results) {
            res.status(201);
            res.send({
                status:201,
                message: "created successfully"
            });
            addpatientreference(patientreferenceinfo);
        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        }); 
     
});


//::ADD
router.post('/addpatientreference', function (req, res) {
    var params = req.body;

     
     
   
     
});

function addpatientreference(patientreferenceinfo){
     params = JSON.parse(patientreferenceinfo);
    var patientid = util.IsNull(params.patientid);
    var referencetype = util.IsNull(params.referencetype); 
    var referername = util.IsNull(params.referername); 
    var referermail = util.IsNull(params.referermail);
    var referermobile= util.IsNull(params.referermobile); 
    var refereraddress= util.IsNull(params.refereraddress); 
    var pincode = util.IsNull(params.refererpincode);  
    var city = util.IsNull(params.referercity); 
    var centerid = util.IsNull(params.centerid); 
    var centeradmincomments = util.IsNull(params.centeradmincomments); 
  

var sql = "INSERT INTO patientreferencelog(patientreferenceid, patientid, referencetype, referername, referermail, referermobile, refereraddress, pincode, city, centerid,centeradmincomments) "
sql += " VALUES(nextval('patientreferenceid_seq'),$1,$2,$3,$4,$5,$6,$7,$8,$9,$10)  RETURNING currval('patientreferenceid_seq');";

db.any(sql, [patientid, referencetype, referername, referermail, referermobile, refereraddress, pincode, city, centerid,centeradmincomments
    ]) .then(function (results) {
        console.log('Create Patient Reference SUCCESS');
        })
        .catch(function (err) {
            console.log('Error Create Patient Reference.',err);
        }); 



}

 function updateMedicineAvailableQuantity(deliverby,develiverQty,medicineid){ 

    var sql = "UPDATE medicine SET availablequantity=(availablequantity-$3),updatedby=$2";
    sql += " WHERE medicineid=$1";  

    db.any(sql, [medicineid, deliverby, develiverQty])
        .then(function (results) {
            console.log('Update Medicine Deliver Quantity',deliverby,develiverQty,medicineid);
        })
        .catch(function (err) {
            console.log('Update Medicine Deliver Quantity ', err);
             
        });
 }

module.exports = router;