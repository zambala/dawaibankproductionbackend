var express = require('express');
var router = express.Router();
var db = require('./dbconnections_controller');
var util = require('./../config/helper');


//console.log(util.responseconfig.SUCCESS);

//::LIST
router.get('/', function (req, res) {

    var params = req.body;
    var offset = util.IsNull(req.query.offset, 0);
    var limit = util.IsNull(req.query.limit, 100);
    var orderby = util.IsNull(req.query.orderby);
    var sortby = util.IsNull(req.query.sortby);
    var status = util.IsNull(req.query.status,1);
    var cityid = util.IsNull(req.query.city);
    var collectioncenterid = util.IsNull(req.query.collectioncenterid);


    var sql = "SELECT * from dropbox where true ";
    if (cityid != null && cityid != "") {
        sql += " AND cityid= $4 "; 
    }
    if (status != null && status != "") {
        sql += " AND status= $3 "; 
    }
    if(collectioncenterid != null  && collectioncenterid != ""){
        sql += " AND collectioncenterid = $5 "; 
    }
    if (orderby != null && orderby != "") {
        sql += " order by " + orderby;
        if (sortby != null && sortby != "") {
            sql += " " + sortby;
        }
    }
    sql += " offset $1 limit $2 ";
   console.log('dropbox sql ',sql,offset, limit,status,cityid,collectioncenterid);
    db.any(sql, [offset, limit,status,cityid,collectioncenterid])
        .then(function (results) {
            res.status(200);
            res.send(results);
        })
        .catch(function (err) {
            console.log('Error Dropbox list',err);
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });

});


//::GETITEM
router.get('/:id', function (req, res) {
     
    var id = req.params.id;

    var sql = "SELECT * FROM dropbox WHERE dropboxid=$1";
    db.any(sql, [id])
        .then(function (results) {
            var result = (results.length > 0) ? results[0] : {}
            res.status(200);
            res.send(result);
        })
        .catch(function (err) {
            // error;
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });
});

//::ADD
router.post('/', function (req, res) {
    var params = req.body;

    var dropboxname =  util.IsNull(params.dropboxname);
    var collectioncenterid = util.IsNull(params.collectioncenterid); 
    var address = util.IsNull(params.address); 
    var status = 1; 
    var addedby = util.IsNull(params.addedby);  
    var cityid = util.IsNull(params.cityid);  

      var sql = "SELECT * FROM dropbox WHERE dropboxname=$1 and collectioncenterid=$2";
       db.any(sql, [dropboxname,collectioncenterid])
        .then(function (results) {

            if (results != null && results.length == 0) {

                var sql = "INSERT INTO dropbox(dropboxid, dropboxname, collectioncenterid, address, status, addedon, addedby,cityid)"
                sql += " VALUES(nextval('dropbox_seq'),$1,$2,$3,$4,now(),$5,$6)";
            
                db.any(sql, [dropboxname ,collectioncenterid ,address,status,addedby,cityid
                    ])
                    .then(function (results) {
                        res.status(201);
                        res.send({
                            status:201,
                            message: "Dropbox created"               
                        });
                    })
                    .catch(function (err) {
                        var code = err.code == 23505 ? 409 : 400
                        res.status(code);
                        res.send(err);
                    });

            } else {
                res.status(200);
                res.send({
                    status:202,
                    message: "Dropbox name is already exist",
                      
                });
            }
        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });  

});

//::UPDATE
router.post('/:id', function (req, res) {
    var params = req.body;

    var id = parseInt(req.params.id); 
    var dropboxname =  util.IsNull(params.dropboxname);
    var collectioncenterid = util.IsNull(params.collectioncenterid); 
    var address = util.IsNull(params.address); 
    var status = util.IsNull(params.status); 
    var updatedby = util.IsNull(params.updatedby);


    var sql = "UPDATE dropbox SET dropboxname =coalesce($2,dropboxname),collectioncenterid =coalesce($3,collectioncenterid),address=coalesce($4,address),";
    sql += " status=coalesce($5,status),updatedby=$6";
    sql += " WHERE dropboxid=$1";
   
    db.any(sql, [id, dropboxname ,collectioncenterid ,address ,status ,updatedby
        ])
        .then(function (results) {
            res.status(200);
            res.send({
                status:201,
                message: "dropbox update",
                id: id
            });
        })
        .catch(function (err) {
            console.log(err);
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });
});
 
module.exports = router;