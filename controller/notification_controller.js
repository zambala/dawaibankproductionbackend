var express = require('express');
var router = express.Router();
var db = require('./dbconnections_controller');
var util = require('./../config/helper');


//console.log(util.responseconfig.SUCCESS);

//::LIST
router.get('/', function (req, res) {

    var params = req.body;
    var offset = util.IsNull(req.query.offset, 0);
    var limit = util.IsNull(req.query.limit, 100);
    var orderby = util.IsNull(req.query.orderby,'createdon');
    var sortby = util.IsNull(req.query.sortby,'desc');
    var status = util.IsNull(req.query.status);
     

    var sql = "SELECT * from notificationinfo where true ";
    
    if (status != null && status != "") {
        sql += " AND status= $3 "; 
    }    
    if (orderby != null && orderby != "") {
        sql += " order by " + orderby;
        if (sortby != null && sortby != "") {
            sql += " " + sortby;
        }
    }
    sql += " offset $1 limit $2 ";
   console.log('notificationinfo sql ',sql);
    db.any(sql, [offset, limit,status])
        .then(function (results) {
            res.status(200);
            res.send(results);
        })
        .catch(function (err) {
            console.log('Error notificationinfo list',err);
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });

});


//::ADD
router.post('/', function (req, res) {

    var params = req.body;
    var message = util.IsNull(params.message)
    var heading = util.IsNull(params.heading);
    var buttontext = util.IsNull(params.buttontext);
    var landingpage = util.IsNull(params.landingpage);
    var sectiontype = util.IsNull(params.sectiontype);
    var notificationtype = util.IsNull(params.notificationtype);

     var sql = "INSERT INTO notificationinfo(id,heading,message,buttontext,landingpage,sectiontype,notificationtype,status,createdon) VALUES ";
    sql += "(nextval('notificationinfo_seq'),$1,$2,$3,$4,$5,$6,true,current_timestamp) RETURNING currval('notificationinfo_seq')";

    var closesql = "Update notificationinfo set status=false";
    db.any(closesql).then(function (closeresult) {
        console.log(closeresult);
        db.any(sql, [heading, message, buttontext, landingpage, sectiontype, notificationtype]).then(function (result) {
            console.log(result);
            res.status(201);
            res.send({
                status: 201,
                message: "success"
            });
        }).catch(function (err) {
            console.log(err);
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        })
    }).catch(function (err) {
        console.log(err);
        var code = err.code == 23505 ? 409 : 400
        res.status(code);
        res.send(err);
    });

});

//::UPDATE
router.post('/:id', function (req, res) {
    var params = req.body;

    var id = parseInt(req.params.id); 
    var dropboxname =  util.IsNull(params.dropboxname);
    var collectioncenterid = util.IsNull(params.collectioncenterid); 
    var address = util.IsNull(params.address); 
    var status = util.IsNull(params.status); 
    var updatedby = util.IsNull(params.updatedby);


    var sql = "UPDATE dropbox SET dropboxname =coalesce($2,dropboxname),collectioncenterid =coalesce($3,collectioncenterid),address=coalesce($4,address),";
    sql += " status=coalesce($5,status),updatedby=$6";
    sql += " WHERE dropboxid=$1";
   
    db.any(sql, [id, dropboxname ,collectioncenterid ,address ,status ,updatedby
        ])
        .then(function (results) {
            res.status(200);
            res.send({
                status:201,
                message: "dropbox update",
                id: id
            });
        })
        .catch(function (err) {
            console.log(err);
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });
});
 
module.exports = router;