//import { strictEqual } from 'assert';

var express = require('express');
var router = express.Router();
var db = require('./dbconnections_controller');
var util = require('./../config/helper');
//var PDFDocument = require('pdfkit'); // For PDF generation http://programmerblog.net/generate-pdf-using-nodejs/
var htmlToPdf = require('html-to-pdf');
htmlToPdf.setDebug(true);
htmlToPdf.setInputEncoding('UTF-8');
htmlToPdf.setOutputEncoding('UTF-8');

var stringInject = require('stringinject');
//var pdftohtml = require('pdftohtmljs');
//var fillPdf = require("fill-pdf");

var fs = require('fs');
var pdf = require('html-pdf');
var html = fs.readFileSync('businesscard.html', 'utf8');
var options = {
    format: 'Letter'
};





var formData = {
    'PAN': '34'
};
var pdfTemplatePath = "CKYC FORM.pdf";

/* 
var fillPdf = require("fill-pdf");
var formDate = { FieldName: 'Text to put into form field' };
var pdfTemplatePath = "formpdfeditable.pdf";
  */
router.get('/pdfconverter1', function (req, res) {

    var chb = '<input type="checkbox" checked class="chbwd" >';
    var chb1 = '<input type="checkbox" checked class="chbwd" >';
    var chb2 = '<input type="text" value="sasi" class="chbwd" >';

    var html = `<html> <head> <meta charset="utf-8"> <title>Individual KYC Form</title> <style> .chbwd{ width: 15px; padding:none; margin-right: -4px; }</style> </head> <body> <div id="container"> <table width="100%" border="1"> <tbody> <tr> <td > Test ${chb}  ${chb1}  ${chb2}</td> </tr> </tbody> </table> </div></body></html> </head>`;

    pdf.create(html, options).toFile('businesscard.pdf', function (err, res) {
        if (err) return console.log(err);
        console.log(res); // { filename: '/app/businesscard.pdf' }
    });


});

/* router.get('/pdfconverter1', function(req, res, next) {
    fillPdf.generatePdf(formData, pdfTemplatePath, function(err, output) {
        if ( !err ) {
          res.type("application/pdf");
          res.send(output);
        }
      });

});  */

//::LOGIN
router.post('/addsymbol', function (req, res) {
    var params = req.body;
    var clientid = params.clientid;
    var watchinfo = JSON.stringify(params.watchinfo);
    console.log(params.watchinfo);

    var sql = " UPDATE clientwatch SET watchinfo = jsonb_set(watchinfo, '{data}'::text[], watchinfo ->'data' || $2::jsonb, TRUE) "
    sql += " WHERE id=$1";

    console.log(sql);
    db.any(sql, [clientid, watchinfo])
        .then(function (results) {
            res.status(200);
            res.send({
                message: "watchinfo update",
                id: clientid
            });
        })
        .catch(function (err) {
            console.log(err);
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });

});

//::delete
router.post('/removesymbol', function (req, res) {
    var params = req.body;
    var clientid = params.clientid;
    var exchange = params.watchinfo.exchange;
    var symbol = params.watchinfo.symbol;


    var sql = " UPDATE clientwatch SET watchinfo = watchinfo #- ('{data,' || ( SELECT i ";
    sql += " FROM generate_series(0, jsonb_array_length(watchinfo->'data') - 1) AS i ";
    sql += "   WHERE (watchinfo->'data'->i->>'exchange' = $2 and watchinfo->'data'->i->>'symbol' = $3) ";
    sql += "  ) || '}')::text[] WHERE id=$1";


    console.log(sql);
    db.any(sql, [clientid, exchange, symbol])
        .then(function (results) {
            res.status(200);
            res.send({
                message: "watchinfo Removed",
                id: clientid
            });
        })
        .catch(function (err) {
            console.log(err);
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });

});


/* router.get('/pdfconverter', function(req, res, next) {
    var pdftohtml = require('pdftohtmljs');
    var converter = new pdftohtml('CKYC.pdf', "sample.html");
    converter.convert('ipad').then(function() {
        console.log("Success");
      }).catch(function(err) {
        console.error("Conversion error: " + err);
      });
       
      // If you would like to tap into progress then create
      // progress handler
      converter.progress(function(ret) {
        console.log ((ret.current*100.0)/ret.total + " %");
      });


}); */

router.post('/pdf', function (req, res, next) {


    var params = req.body;
    var first_name = (params.firstname);
    var pan_no = (params.pan);
    var kyc = '';
    var dob = (params.otherinfo.APP_DOB_DT);
    var father_name = (params.otherinfo.APP_F_NAME);
    var mother_name = (params.otherinfo.APP_MOTHER_FNAME);
    var guaridan_name = (params.nominationinfo.nom_guardianData);
    var guardian_pan = (params.nominationinfo.nom_guardianpan);
    var contact_address = (params.addressinfo.APP_COR_ADD1);
    var city = (params.addressinfo.APP_COR_CITY);
    var pincode = (params.addressinfo.APP_COR_PINCD);
    var state = (params.addressinfo.APP_COR_STATE);
    var country = (params.addressinfo.APP_COR_CTRY);
    var email = (params.contactinfo.APP_APP_EMAIL);
    var mobile_no_prefix = (params.contactinfo.mobileCountryCode);
    var mobile_no = (params.mobilenumber);
    var income_slab = (params.addressinfo.APP_COR_CTRY);
    var occupation_details = (params.otherinfo.occupationDetails);
    var place_ofbirth = (params.otherinfo.APP_BIRTH_PLACE);
    var country_taxresidence = (params.addressinfo.APP_COR_CTRY);
    var tax_no = (params.otherinfo.first_app_taxid);
    var politically_yes = (params.otherinfo.first_app_politicalperson);
    var politically_no = '';
    var holding_type = (params.otherinfo.hold_type);
    var second_app_name = (params.otherinfo.second_app_name);
    var second_app_pan = (params.otherinfo.second_app_pan);
    var second_app_dob = (params.otherinfo.second_app_dob);
    var second_app_income = (params.otherinfo.second_app_income);
    var second_app_occ = (params.otherinfo.second_app_occ);
    var second_app_placeOfBirth = (params.otherinfo.second_app_placeOfBirth);
    var second_app_ctry = (params.otherinfo.second_app_ctry);
    var second_app_taxid = (params.otherinfo.second_app_taxid);
    var third_app_name = (params.otherinfo.third_app_name);
    var third_app_pan = (params.otherinfo.third_app_pan);
    var third_app_dob = (params.otherinfo.third_app_dob);
    var third_app_income = (params.otherinfo.third_app_income);
    var third_app_occ = (params.otherinfo.third_app_occ);
    var third_app_placeOfBirth = (params.otherinfo.third_app_placeOfBirth);
    var third_app_ctry = (params.otherinfo.third_app_ctry);
    var third_app_taxid = (params.otherinfo.third_app_taxid);
    var bank_accno = (params.bankinfo.bankAccNo);
    var bank_name = (params.bankinfo.bankName);
    var bank_branch = (params.bankinfo.bankBranch);
    var bank_accounttype = (params.bankinfo.bankAccType);
    var bank_ifsccode = (params.bankinfo.bankIFSCcode);
    var bank_address = (params.bankinfo.bankAddress);
    var bank_city = (params.bankinfo.bankCity);
    var bank_pincode = (params.bankinfo.bankPincode);
    var bank_state = (params.bankinfo.bankState);
    var bank_country = (params.bankinfo.bankCountry);
    var nominee_name = (params.nominationinfo.nom_name);
    var nominee_relation = (params.nominationinfo.nom_relationship);
    var nominee_guaridan_name = (params.nominationinfo.nom_guardianData);
    var nominee_guaridan_address = (params.nominationinfo.nom_address);
    var nominee_city = (params.nominationinfo.nominee_city);
    var nominee_pincode = (params.nominationinfo.nominee_pincode);
    var nominee_state = (params.nominationinfo.nominee_state);
    var application_date = '';


    var html = `<html> <head> <meta charset="utf-8"> <title>Account Open Form - Zambala Mutual Fund</title> </head> <style > table { border-collapse: collapse; } th, td { border: 1px solid black; padding: 5px; text-align: left; font-size:12px } 
   #headerText{ font-size:20px; padding:15px;} </style> <body><table width="100%"> <tbody> <tr> <td  colspan="7" id="headerText">ZAMBALA</td> </tr> <tr> <td colspan="4"> Broker/Agent code ARN: ARN - 18477 </td> <td colspan="2"> SUB-BROKER:</td> <td width="13%"> EUIN: E180789</td> </tr> <tr> <td colspan="7"> Unit Holder Information</td> </tr> <tr> <td colspan="7"> Name of First Applicant:${first_name}</td> </tr> <tr> <td colspan="3"> PAN Number:${pan_no}</td> <td colspan="2"> KYC:${kyc}</td> <td colspan="2"> Date of Birth:${dob}</td> </tr><tr> <td colspan="5"> Father Name: ${father_name}  </td> <td colspan="2"> Mother Name:${mother_name}</td> </tr> <tr> <td colspan="5"> Name of Guardian:${guaridan_name} </td> <td colspan="2"> PAN:${guardian_pan}</td> </tr> <tr> <td colspan="7"> Contact Address:${contact_address}</td> </tr> <tr> <td colspan="4">City:${city}</td> <td width="15%">Pin Code:${pincode}</td> <td width="15%">State:${state}</td> <td>Country:${country}</td> </tr> <tr> <td colspan="4">Tel (Off):</td> <td width="15%">Tel (Res):</td> <td colspan="2">Email:${email}</td> </tr> <tr> <td colspan="4">Fax(Off):</td> <td width="15%">Fax(Res):</td> <td colspan="2">Mobile:${mobile_no_prefix}${mobile_no}</td> </tr> <tr> <td colspan="5">Income Tax Slab/Networth:${income_slab}</td> <td colspan="2">Occupation Detail:${occupation_details}</td> </tr> <tr> <td colspan="3">Place of Birth:${place_ofbirth}</td> <td colspan="4">Country of Tax Residence:${country_taxresidence}</td> </tr> <tr> <td colspan="7">Tax id No:${tax_no}</td> </tr> <tr> <td colspan="5">Politically exposed person / Related to Politically exposed person etc ?</td> <td>Yes &#9745; <input type="checkbox" ${politically_yes} ></td> <td>No<input type="checkbox" ${politically_no} ></td> </tr> <tr> <td colspan="5">Mode of Holding:${holding_type} </td> <td colspan="2">Occupation: ${occupation_details}</td> </tr> <tr> <td colspan="3">Place of Birth:${place_ofbirth}</td> <td colspan="4">Country of Tax Residence:${country_taxresidence}</td> </tr> <tr> <td colspan="7">Tax id No:${tax_no}</td> </tr> <tr> <td colspan="5">Politically exposed person / Related to Politically exposed person etc ?</td> <td>Yes <input type="checkbox" checked ${politically_yes} ></td> <td>No<input type="checkbox" ${politically_no} ></td> </tr> <tr> <td colspan="5">Mode of Holding:${holding_type} </td> <td colspan="2">Occupation:</td> </tr> <tr> <td colspan="5">Name of Second Applicant:${second_app_name}</td> <td colspan="2">&nbsp;</td> </tr> <tr> <td colspan="4">PAN Number:${second_app_pan}</td> <td>KYC:</td> <td colspan="2">Date of Birth:${second_app_dob}</td> </tr> <tr> <td colspan="5">Income Tax Slab/Networth: ${second_app_income}</td> <td colspan="2">Occupation Detail: ${second_app_occ}</td> </tr> <tr> <td colspan="4">Place of Birth:${second_app_placeOfBirth}</td> <td colspan="3">Country of Tax Residence:${second_app_ctry} </td> </tr> <tr> <td colspan="7">Tax id No:${second_app_taxid}</td> </tr> <tr> <td colspan="5">Politically exposed person / Related to Politically exposed person etc ?</td> <td><span> Yes &#10004;</span></td> <td>No</td> </tr> <tr> <td colspan="5">Name of Third Applicant:${third_app_name}</td> <td colspan="2">&nbsp;</td> </tr> <tr> <td colspan="4">PAN Number:${third_app_pan}</td> <td>KYC:</td> <td colspan="2">Date of Birth:${third_app_dob}</td> </tr> <tr> <td colspan="5">Income Tax Slab/Networth:${third_app_income}</td> <td colspan="2">Occupation Detail:${third_app_occ}</td> </tr> <tr> <td colspan="4">Place of Birth:${third_app_dob}</td> <td colspan="3">Country of Tax Residence:${third_app_ctry} </td> </tr> <tr> <td colspan="7">Tax id No:${third_app_taxid}</td> </tr> <tr> <td colspan="5">Politically exposed person / Related to Politically exposed person etc ?</td> <td>Yes  </td> <td>No</td> </tr> <tr> <td colspan="7">Other Details of sole / 1st Applicant</td> </tr> <tr> <td colspan="7"><p>Overseas Address:</p> <p>(Incase of NRI Investor)</p></td> </tr> <tr> <td width="27%">City:</td> <td colspan="2">Pincode:</td> <td colspan="4">Country:</td> </tr><tr> <td colspan="7">Bank Mandate Details</td> </tr> <tr> <td colspan="3">Name of Bank:${bank_name}</td> <td colspan="4">Branch:${bank_branch}</td> </tr> <tr> <td>A/C No:${bank_accno}</td> <td colspan="2">A/C Type:${bank_accounttype}</td> <td colspan="4">IFSC Code:${bank_ifsccode}</td> </tr> <tr> <td colspan="7">Bank Address:${bank_address}</td> </tr> <tr> <td>City:${bank_city}</td> <td colspan="2">Pincode:${bank_pincode}</td> <td colspan="2">State:${bank_state}</td> <td colspan="2">Country:${bank_country}</td> </tr> <tr> <td colspan="7">Nomination Details</td> </tr> <tr> <td colspan="3">Nominee Name: ${nominee_name}</td> <td colspan="4">Relationship:${nominee_relation}</td> </tr> <tr> <td colspan="7">Guardian Name (If Nominee is Minor):${nominee_guaridan_name} </td> </tr> <tr> <td colspan="7">Nominee Address:${nominee_guaridan_address}</td> </tr> <tr> <td>City:${nominee_city}</td> <td colspan="2">Pincode:${nominee_pincode}</td> <td colspan="4">State:${nominee_state}</td> </tr> <tr> <td colspan="7"><p>Declaration and Signature</p> <p>I/We confirm that details provided by me/us are true and correct. The ARN holder has disclosed to me/us all the commission (In the form of trail commission or any other mode), payable to him for the different competing Schemes of various Mutual Fund from amongst which the scheme is being recommended to me/us.</p></td> </tr> <tr> <td colspan="2">Date:${application_date}</td> <td colspan="5">Place:</td> </tr> <tr> <td>&nbsp;</td> <td colspan="2">&nbsp;</td> <td colspan="4"><p>&nbsp;</p> <p>&nbsp;</p></td> </tr>  <tr> <td>1st Applicant Signature:</td> <td colspan="2">2nd Applicant Signature:</td> <td colspan="4">3rd Applicant Signature:</td> </tr></tbody> </table></body></html>`;



    //console.log(html);

    htmlToPdf.convertHTMLString(html, 'test.pdf',
        function (error, success) {
            if (error) {
                console.log('Oh noes! Errorz!');
                console.log(error);
            } else {
                console.log('Woot! Success!');
                console.log(success);
            }
        }
    );


});


router.get('/pdf-newindividualFrm', function (req, res, next) {



    var params = req.body;
    var id = "AQNPB9151A";

    var sql = "SELECT * FROM kycinfo"
    sql += " WHERE pan=$1"
    db.any(sql, [id])
        .then(function (resultdata) {
            var cssCls = 'chbwd';
            var results = resultdata[0];
            var pan_no = buildPdfInput(results.pan, 9, cssCls);
            var application_new = '<input type="checkbox" checked class="' + cssCls + '" >';
            var kyctype_normal = '';
            var applicant_name_prefix = buildPdfInput('', 3, cssCls);
            var applicant_firstname = buildPdfInput(results.identityinfo.APP_NAME, 11, cssCls);
            var applicant_lastname = buildPdfInput('', 11, cssCls);
            var applicant_middlename = buildPdfInput('', 11, cssCls);
            var maiden_name_prefix = buildPdfInput('', 3, cssCls);
            var maiden_firstname = buildPdfInput('', 11, cssCls);
            var maiden_lastname = buildPdfInput('', 11, cssCls);
            var maiden_middlename = buildPdfInput('', 11, cssCls);
            var father_name_prefix = buildPdfInput('', 3, cssCls);
            var father_firstname = buildPdfInput(results.identityinfo.APP_F_NAME, 11, cssCls);
            var father_lastname = buildPdfInput(results.identityinfo.APP_F_LNAME, 11, cssCls);
            var father_middlename = buildPdfInput('', 11, cssCls);
            var mother_name_prefix = buildPdfInput('', 3, cssCls);
            var mother_firstname = buildPdfInput(results.identityinfo.APP_MOTHER_FNAME, 11, cssCls);
            var mother_lastname = buildPdfInput(results.identityinfo.APP_MOTHER_LNAME, 11, cssCls);
            var mother_middlename = buildPdfInput('', 11, cssCls);
            var dob = buildPdfInputDate(results.identityinfo.APP_DOB_DT, 11, cssCls);
            var gender_male = (results.identityinfo.APP_GEN == 'M') ? buildCheckBox(true, cssCls) : buildCheckBox(false, cssCls);
            var gender_female = (results.identityinfo.APP_GEN == 'F') ? buildCheckBox(true, cssCls) : buildCheckBox(false, cssCls);
            var gender_transgender = (results.identityinfo.APP_GEN == 'T') ? buildCheckBox(true, cssCls) : buildCheckBox(false, cssCls);
            var marital_married = (results.identityinfo.APP_MAR_STATUS == 01) ? buildCheckBox(true, cssCls) : buildCheckBox(false, cssCls);
            var marital_unmarried = (results.identityinfo.APP_MAR_STATUS == 02) ? buildCheckBox(true, cssCls) : buildCheckBox(false, cssCls);
            var marital_others = buildCheckBox(false, cssCls);
            var country_in = (results.identityinfo.APP_NATIONALITY == 01) ? buildCheckBox(true, cssCls) : buildCheckBox(false, cssCls);
            var othercountry_name = '';
            var othercountry_code = '';
            var resident_individual = (results.identityinfo.APP_RES_STATUS == 'R') ? buildCheckBox(true, cssCls) : buildCheckBox(false, cssCls);
            var resident_nonindividual = (results.identityinfo.APP_RES_STATUS == 'N') ? buildCheckBox(true, cssCls) : buildCheckBox(false, cssCls);
            var foreign_national = (results.identityinfo.APP_RES_STATUS == 'P') ? buildCheckBox(true, cssCls) : buildCheckBox(false, cssCls);
            var person_indianorigin = buildCheckBox(false, cssCls);
            var occ_service_pri = (results.identityinfo.APP_OCCUPATION_CODE == '01') ? buildCheckBox(true, cssCls) : buildCheckBox(false, cssCls);
            var occ_service_pub = (results.identityinfo.APP_OCCUPATION_CODE == '02') ? buildCheckBox(true, cssCls) : buildCheckBox(false, cssCls);
            var occ_service_gov = (results.identityinfo.APP_OCCUPATION_CODE == 11) ? buildCheckBox(true, cssCls) : buildCheckBox(false, cssCls);
            var occ_service_others = (results.identityinfo.APP_OCCUPATION_CODE == 99) ? buildCheckBox(true, cssCls) : buildCheckBox(false, cssCls);
            var occ_service_selfemp = buildCheckBox(false, cssCls);
            var occ_service_housewife = buildCheckBox(false, cssCls);
            var occ_service_business = buildCheckBox(false, cssCls);
            var occ_service_notcategorised = buildCheckBox(false, cssCls);

            var proof_passport = (results.otherproof.APP_DOC_PROOF == 03) ? buildCheckBox(true, cssCls) : buildCheckBox(false, cssCls);
            var proof_passport_no = (results.otherproof.APP_DOC_PROOF == 03) ? buildPdfInput(results.otherproof.proof_identity_no, 9, cssCls) : buildPdfInput('', 11, cssCls);
            var proof_passport_exp = (results.otherproof.APP_DOC_PROOF == 03) ? buildPdfInputDate(results.otherproof.proof_identity_expdate, cssCls) : buildPdfInput('', cssCls);
            var proof_voterid = (results.otherproof.APP_DOC_PROOF == 05) ? buildCheckBox(true, cssCls) : buildCheckBox(false, cssCls);
            var proof_voterid_no = (results.otherproof.APP_DOC_PROOF == 05) ? buildPdfInput(results.otherproof.proof_identity_no, 14, cssCls) : buildPdfInput('', 11, cssCls);
            var proof_drivingLicence = (results.otherproof.APP_DOC_PROOF == 04) ? buildCheckBox(true, cssCls) : buildCheckBox(false, cssCls);
            var proof_drivingLicence_no = (results.otherproof.APP_DOC_PROOF == 04) ? buildPdfInput(results.otherproof.proof_identity_no, 15, cssCls) : buildPdfInput('', 11, cssCls);
            var proof_drivingLicence_exp = (results.otherproof.APP_DOC_PROOF == 04) ? buildPdfInputDate(results.otherproof.proof_identity_expdate, cssCls) : buildPdfInput('', cssCls);
            var proof_aadharcard = (results.otherproof.APP_DOC_PROOF == 02) ? buildCheckBox(true, cssCls) : buildCheckBox(false, cssCls);
            var proof_aadharcard_no = (results.otherproof.APP_DOC_PROOF == 02) ? buildPdfInput(results.otherproof.proof_identity_no, 12, cssCls) : buildPdfInput('', 11, cssCls);
            var proof_nregajobcard = '';
            var proof_nregajobcard_no = '';
            var proof_otherdoc = '';
            var address_permanent = '';
            var address_permanent_1 = buildPdfInput(results.addressinfo.APP_PER_ADD1, 46, cssCls);
            var address_permanent_2 = buildPdfInput(results.addressinfo.APP_PER_ADD2, 46, cssCls);
            var address_permanent_3 = buildPdfInput(results.addressinfo.APP_PER_ADD3, 29, cssCls);
            var address_permanent_city = buildPdfInput(results.addressinfo.APP_PER_CITY, 9, cssCls);
            var address_permanent_district = '';
            var address_permanent_zipcode = buildPdfInput(results.addressinfo.APP_PER_PINCD, 6, cssCls);
            var address_permanent_statecode = '';
            var address_permanent_state = buildPdfInput(results.addressinfo.APP_PER_STATE, 16, cssCls);
            var address_permanent_country = buildPdfInput(results.addressinfo.APP_PER_CTRY, 12, cssCls);
            var address_permanent_country_code = '';
            var address_permanent_typeResBusiness = '';
            var address_permanent_typeResidential = '';
            var address_permanent_typeBusiness = '';
            var address_permanent_typeRegOffice = '';
            var address_permanent_typeUnsepecified = '';
            var proof_passport_permanent = '';
            var proof_passport_no_permanent = '';
            var proof_passport_exp_permanent = '';
            var proof_voterid_permanent = '';
            var proof_voterid_no_permanent = '';
            var proof_drivingLicence_permanent = '';
            var proof_drivingLicence_no_permanent = '';
            var proof_drivingLicence_exp_permanent = '';
            var proof_aadharcard_permanent = '';
            var proof_aadharcard_no_permanent = '';
            var proof_nregajobcard_permanent = '';
            var proof_nregajobcard_no_permanent = '';
            var proof_otherdoc_permanent = '';
            var address_localaddress = '';
            var address_localaddress_1 = buildPdfInput(results.addressinfo.APP_COR_ADD1, 46, cssCls);
            var address_localaddress_2 = buildPdfInput(results.addressinfo.APP_COR_ADD2, 46, cssCls);
            var address_localaddress_3 = buildPdfInput(results.addressinfo.APP_COR_ADD3, 29, cssCls);
            var address_localaddress_city = buildPdfInput(results.addressinfo.APP_COR_CITY, 9, cssCls);
            var address_localaddress_district = '';
            var address_localaddress_zipcode = buildPdfInput(results.addressinfo.APP_COR_PINCD, 6, cssCls);
            var address_localaddress_statecode = '';
            var address_localaddress_state = buildPdfInput(results.addressinfo.APP_COR_STATE, 16, cssCls);
            var address_localaddress_country = buildPdfInput(results.addressinfo.APP_COR_CTRY, 12, cssCls);
            var address_localaddress_country_code = buildPdfInput(results.addressinfo.APP_COR_CTRY, 2, cssCls);
            var emailid = buildPdfInput(results.contactinfo.APP_EMAIL, 46, cssCls);
            var mobileno = buildPdfInput(results.contactinfo.APP_MOB_NO, 10, cssCls);
            var telephone_off = buildPdfInput(results.contactinfo.APP_TELEPHONE_NO, 8, cssCls);
            var telephone_res = buildPdfInput('', 16, cssCls);
            var fatca_resident = '';
            var fatca_country_jurisdiction = buildPdfInput('', 12, cssCls);
            var fatca_countrycode_jurisdiction = buildPdfInput('', 2, cssCls);
            var fatca_taxidentification_no = buildPdfInput('', 14, cssCls);
            var fatca_city = buildPdfInput('', 12, cssCls);
            var fatca_country = buildPdfInput('', 12, cssCls);;
            var fatca_countrycode = buildPdfInput('', 2, cssCls);
            var fatca_address1 = buildPdfInput('', 46, cssCls);
            var fatca_address2 = buildPdfInput('', 46, cssCls);
            var fatca_address3 = buildPdfInput('', 29, cssCls);
            var fatca_city = buildPdfInput('', 9, cssCls);
            var fatca_district = '';
            var fatca_postcalcode = buildPdfInput('', 6, cssCls);
            var fatca_statecode = buildPdfInput('', 12, cssCls);
            var fatca_state = '';
            var fatca_country = buildPdfInput('', 12, cssCls);
            var fatca_country_code = buildPdfInput('', 2, cssCls);
            var relatedPerson_relatedPerson = '';
            var relatedPerson_deletion = '';
            var relatedPerson_kycno = '';
            var relatedPerson_guardian = '';
            var relatedPerson_assignee = '';
            var relatedPerson_authorised = '';

            var guardian_prefixname = buildPdfInput('', 3, cssCls);
            var guardian_firstname = buildPdfInput('', 11, cssCls);
            var guardian_lastname = buildPdfInput('', 11, cssCls);
            var guardian_middlename = buildPdfInput('', 11, cssCls);

            var guardian_proof = '';
            var guardian_proof_passport = '';
            var guardian_proof_passport_no = '';
            var guardian_proof_passport_exp = '';
            var guardian_proof_voterid = '';
            var guardian_proof_voterid_no = '';
            var guardian_proof_drivingLicence = '';
            var guardian_proof_drivingLicence_no = '';
            var guardian_proof_drivingLicence_exp = '';
            var guardian_proof_aadharcard = '';
            var guardian_proof_aadharcard_no = '';
            var guardian_proof_nregajobcard = '';
            var guardian_proof_nregajobcard_no = '';
            var guardian_proof_otherdoc = '';
            var remarks1 = buildPdfInput('', 46, cssCls);
            var remarks2 = buildPdfInput('', 46, cssCls);
            var application_date = '';
            var application_place = '';



            var html = `<!doctype html> <html> <head> <meta charset="utf-8"> <title>Individual KYC Form</title> <style> body{ font-family: "Arial"; font-size:12px; }.headingsec{ background-color: #f4f1f1; }	 table tr{ border:none; } table tr td{ border:none; } .chbwd{ width: 15px; padding:none; margin-right: -4px; } #container{ background: #183884; padding: 10px; }  table{background-color: white;}</style> </head> <body> <div id="container"><form> <table width="100%" border="1"> <tbody> <tr> <td colspan="5" rowspan="3">Know Your Client (KYC)  Application Form (For Individuals only)  (Please fill the form in English and in BLOCK Letters)  Fields marked with '*' are mandatory fields</td> <td width="10%" height="30">Application  </td> <td colspan="7">New ${application_new} </td> </tr> <tr> <td>Type</td> <td colspan="7">Update   KYC Number </td> </tr> <tr> <td>KYC Type</td> <td colspan="7"> ${kyctype_normal}Normal (PAN is mandatory) PAN Exempt Investors (Refer instruction K)</td> </tr> <tr class="headingsec"> <td colspan="13"> 1. Identity Details (Please refer instruction A at the end)</td> </tr> <tr> <td colspan="13"> PAN ${pan_no} Please enclose a duly attested copy of your PAN Card </td> </tr> <tr> <td colspan="3">&nbsp;</td> <td >Prefix</td> <td>First Name</td> <td>Last Name</td> <td colspan="7">Middle Name</td> </tr> <tr> <td colspan="3">Name* (same as ID proof)</td> <td>${applicant_name_prefix}</td> <td>${applicant_firstname}</td> <td>${applicant_lastname}</td> <td colspan="7">${applicant_middlename}</td> </tr> <tr> <td colspan="3">Maiden Name (If any*)</td> <td>${maiden_name_prefix}</td> <td>${maiden_firstname}</td> <td>${maiden_lastname}</td> <td colspan="7">${maiden_middlename}</td> </tr> <tr> <td colspan="3">Father / Spouse Name*</td> <td>${father_name_prefix}</td> <td>${father_firstname}</td> <td>${father_lastname}</td> <td colspan="7">${father_middlename}</td> </tr> <tr> <td colspan="3">Mother Name*</td> <td>${mother_name_prefix}</td> <td>${mother_firstname}</td> <td>${mother_lastname}</td> <td colspan="7">${mother_middlename}</td> </tr> <tr> <td colspan="3">Date of Birth*</td> <td colspan="9">${dob}</td> <td width="22%" rowspan="9">&nbsp;</td> </tr> <tr> <td colspan="3">Gender*</td> <td colspan="3">${gender_male}M- Male</td> <td width="18%" colspan="3">${gender_female}F- Female</td> <td colspan="3">${gender_transgender}T-Transgender</td> </tr> <tr> <td colspan="3">Marital Status*</td> <td colspan="3">${marital_married}Married</td> <td colspan="3">${marital_unmarried} Unmarried</td> <td colspan="3">${marital_others}Others</td> </tr> <tr> <td colspan="3">Citizenship*</td> <td colspan="3">${country_in}IN- Indian </td> <td colspan="3">${othercountry_name}Others – Country</td> <td colspan="3">${othercountry_code} Country Code</td> </tr> <tr> <td colspan="3" rowspan="2">Residential Status*</td> <td colspan="3">${resident_individual}Resident Individual </td> <td colspan="3">${resident_nonindividual}Non Resident Indian</td> <td colspan="3">&nbsp;</td> </tr> <tr> <td colspan="3">${foreign_national}Foreign National</td> <td colspan="3">${person_indianorigin}Person of Indian Origin</td> <td colspan="3">&nbsp;</td> </tr> <tr> <td colspan="3" rowspan="3">Occupation Type*</td> <td colspan="3"> ${occ_service_pri}S-Service Private Sector</td> <td colspan="3"> ${occ_service_pub}Public Sector</td> <td colspan="3"> ${occ_service_gov}Government Sector</td> </tr> <tr> <td colspan="3">${occ_service_others}O-Others Professional</td> <td colspan="3"> ${occ_service_selfemp}Self Employed Retired </td> <td colspan="3">${occ_service_housewife}Housewife Student</td> </tr> <tr> <td colspan="3">${occ_service_business}B-Business</td> <td colspan="3">${occ_service_notcategorised} X-Not Categorised</td> <td colspan="3">&nbsp;</td> </tr> <tr class="headingsec"> <td colspan="13">2. Proof of Identity (PoI)* (for PAN exempt Investor or if PAN card copy not provided) (Please refer instruction C & K at the end)</td> </tr> <tr> <td colspan="13">(Certified copy of any one of the following Proof of Identity [PoI] needs to be submitted)</td> </tr> <tr> <td colspan="3">${proof_passport}A- Passport Number</td> <td colspan="6">${proof_passport_no}</td> <td colspan="3">Passport Expiry Date</td> <td>${proof_passport_exp}</td> </tr> <tr> <td colspan="3">${proof_voterid}B- Voter ID Card</td> <td colspan="6">${proof_voterid_no}</td> <td colspan="4">&nbsp;</td> </tr> <tr> <td colspan="3">${proof_drivingLicence}D- Driving Licence</td> <td colspan="6">${proof_drivingLicence_no}</td> <td colspan="3">Driving Licence Expiry Date</td> <td>${proof_drivingLicence_exp}</td> </tr> <tr> <td colspan="3">${proof_aadharcard}E- Aadhaar Card</td> <td colspan="6">${proof_aadharcard_no}</td> <td colspan="4" rowspan="2">&nbsp;</td> </tr> <tr> <td colspan="3">${proof_nregajobcard}F- NREGA Job Card</td> <td colspan="6">${proof_nregajobcard_no}</td> </tr> <tr> <td colspan="9">${proof_otherdoc}Z- Others (any document notified by the central government)</td> <td colspan="3">Identification Number</td> <td>&nbsp;</td> </tr> <tr> <td colspan="13">3. Proof of Address (PoA)*</td> </tr> <tr> <td colspan="13" class="headingsec">${address_permanent}3.1 Current / Permanent / Overseas Address Details (Please see instruction D at the end)</td> </tr> <tr> <td colspan="13">Address</td> </tr> <tr> <td width="8%" colspan="2">Line 1*</td> <td colspan="11">${address_permanent_1}</td> </tr> <tr> <td colspan="2">Line 2</td> <td colspan="11">${address_permanent_2}</td> </tr> <tr> <td colspan="2">Line 3</td> <td colspan="7">${address_permanent_3}</td> <td colspan="3">City / Town / Village*</td> <td>${address_permanent_city}</td> </tr> <tr> <td colspan="2">District*</td> <td colspan="3">${address_permanent_district}</td> <td colspan="4">Zip / Post Code* ${address_permanent_zipcode}</td> <td colspan="4">State/UT Code ${address_permanent_statecode} as per Indian Motor Vehicle Act, 1988</td> </tr> <tr> <td colspan="2">State/UT*</td> <td colspan="4">${address_permanent_state}</td> <td colspan="5">Country* ${address_permanent_country}</td> <td colspan="2">Country Code ${address_permanent_country_code} as per ISO 3166</td> </tr> <tr> <td colspan="2">Address Type*</td> <td colspan="2">${address_permanent_typeResBusiness} Residential / Business </td> <td colspan="2"> ${address_permanent_typeResidential}Residential </td> <td colspan="4"> ${address_permanent_typeBusiness}Business</td> <td colspan="2"> ${address_permanent_typeRegOffice} Registered Office </td> <td>Unspecified ${address_permanent_typeUnsepecified}</td> </tr> <tr> <td colspan="13">(Certified copy of any one of the following Proof of Address [PoA] needs to be submitted)</td> </tr> <tr> <td colspan="3">${proof_passport_permanent}A- Passport Number</td> <td colspan="6">${proof_passport_no_permanent}</td> <td colspan="3">Passport Expiry Date</td> <td>${proof_passport_exp_permanent}</td> </tr> <tr> <td colspan="3">${proof_voterid_permanent}B- Voter ID Card</td> <td colspan="6">${proof_voterid_no_permanent}</td> <td colspan="4">&nbsp;</td> </tr> <tr> <td colspan="3">${proof_drivingLicence_permanent}D- Driving Licence</td> <td colspan="6">${proof_drivingLicence_no_permanent}</td> <td colspan="3">Driving Licence Expiry Date</td> <td>${proof_drivingLicence_exp_permanent}</td> </tr> <tr> <td colspan="3">${proof_aadharcard_permanent}E- Aadhaar Card</td> <td colspan="6">${proof_aadharcard_no_permanent}</td> <td colspan="4" rowspan="2">&nbsp;</td> </tr> <tr> <td colspan="3">${proof_nregajobcard_permanent}F- NREGA Job Card</td> <td colspan="6">${proof_nregajobcard_no_permanent}</td> </tr> <tr> <td colspan="9">${proof_otherdoc_permanent}Z- Others (any document notified by the central government)</td> <td colspan="3">Identification Number</td> <td>&nbsp;</td> </tr> <tr> <td colspan="13" class="headingsec">${address_localaddress}3.2 Correspondence / Local Address Details* (Please see instruction E at the end)</td> </tr> <tr> <td colspan="13">Same as Current / Permanent / Overseas Address details (In case of multiple correspondence / local addresses, please fill &lsquo;Annexure A1&rsquo;, Submit relevant documentary proof)</td> </tr> <tr> <td width="8%" colspan="2">Line 1*</td> <td colspan="11">${address_localaddress_1}</td> </tr> <tr> <td colspan="2">Line 2</td> <td colspan="11">${address_localaddress_2}</td> </tr> <tr> <td colspan="2">Line 3</td> <td colspan="7">${address_localaddress_3}</td> <td colspan="3">City / Town / Village*</td> <td>${address_localaddress_city}</td> </tr> <tr> <td colspan="2">District*</td> <td colspan="3">${address_localaddress_district}</td> <td colspan="4">Zip / Post Code* ${address_localaddress_zipcode}</td> <td colspan="4">State/UT Code ${address_localaddress_statecode} as per Indian Motor Vehicle Act, 1988</td> </tr> <tr> <td colspan="2">State/UT*</td> <td colspan="4">${address_localaddress_state}</td> <td colspan="5">Country* ${address_localaddress_country}</td> <td colspan="2">Country Code ${address_localaddress_country_code} as per ISO 3166</td> </tr> <tr> <td height="31" colspan="13" class="headingsec">4. Contact Details (All communications will be sent on provided Mobile no. / Email-ID) (Please refer instruction F at the end)</td> </tr> <tr> <td colspan="2">Email ID</td> <td colspan="11">${emailid}</td> </tr> <tr> <td colspan="2">Mobile</td> <td colspan="3">${mobileno}</td> <td colspan="6">Tel. (Off) ${telephone_off}</td> <td colspan="2">Tel. (Res) ${telephone_res}</td> </tr> <tr> <td colspan="13" class="headingsec">5. FATCA/CRS Information (Tick if Applicable) ${fatca_resident} Residence for Tax Purposes in Jurisdiction(s) Outside India (Please refer instruction B at the end)</td> </tr> <tr> <td colspan="13">Additional Details Required* (Mandatory only if above option (5) is ticked)</td> </tr> <tr> <td colspan="4">Country of Jurisdiction of Residence*</td> <td colspan="5">${fatca_country_jurisdiction} </td> <td colspan="4">Country Code of Jurisdiction of Residence as per ${fatca_countrycode_jurisdiction}  ISO 3166</td> </tr> <tr> <td colspan="13">Tax Identification Number or equivalent (If issued by jurisdiction)*</td> </tr> <tr> <td colspan="13">${fatca_taxidentification_no}</td> </tr> <tr> <td colspan="3">Place / City of Birth*</td> <td colspan="3">${fatca_city}</td> <td colspan="5">Country of Birth* ${fatca_country}</td> <td colspan="2">Country Code ${fatca_countrycode}as per ISO 3166</td> </tr> <tr> <td colspan="13">Address</td> </tr> <tr> <td width="8%" colspan="2">Line 1*</td> <td colspan="11">${fatca_address1}</td> </tr> <tr> <td colspan="2">Line 2</td> <td colspan="11">${fatca_address2}</td> </tr> <tr> <td colspan="2">Line 3</td> <td colspan="7">${fatca_address3}</td> <td colspan="3">City / Town / Village*</td> <td>${fatca_city}</td> </tr> <tr> <td colspan="2">District*</td> <td colspan="3">${fatca_district}</td> <td colspan="4">Zip / Post Code* ${fatca_postcalcode}</td> <td colspan="4">State/UT Code ${fatca_statecode}as per Indian Motor Vehicle Act, 1988</td> </tr> <tr> <td colspan="2">State/UT*</td> <td colspan="4">${fatca_state}</td> <td colspan="5">Country* ${fatca_country}</td> <td colspan="2">Country Code  ${fatca_country_code}as per ISO 3166</td> </tr> <tr> <td colspan="13" class="headingsec">6. Details of Related Person (Optional) (please refer instruction G at the end) (in case of additional related persons, please fill &lsquo;Annexure B1&rsquo;)</td> </tr> <tr> <td colspan="4"> ${relatedPerson_relatedPerson}Related Person</td> <td colspan="5"> ${relatedPerson_deletion}Deletion of Related Person</td> <td colspan="4"> ${relatedPerson_kycno}KYC Number of Related Person (if available*)</td> </tr> <tr> <td colspan="4">Related Person Type*</td> <td colspan="2">${relatedPerson_guardian}Guardian of Minor</td> <td colspan="4">${relatedPerson_assignee}Assignee</td> <td colspan="3">${relatedPerson_authorised}Authorized Representative</td> </tr> <tr> <td colspan="3">&nbsp;</td> <td width="7%">Prefix</td> <td width="8%">First Name</td> <td>Last Name</td> <td colspan="7">Middle Name</td> </tr> <tr> <td colspan="3">Name* (same as ID proof)</td> <td>${guardian_prefixname}</td> <td>${guardian_firstname}</td> <td>${guardian_lastname}</td> <td colspan="7">${guardian_middlename}</td> </tr> <tr> <td colspan="3">&nbsp;</td> <td colspan="10">(If KYC number and name are provided, below details of section 6 are optional)</td> </tr> <tr> <td colspan="13">${guardian_proof}Proof of Identity [PoI] of Related Person* (Please see instruction (H) at the end)</td> </tr> <tr> <td colspan="13">(Certified copy of any one of the following Proof of Identity[PoI] needs to be submitted)</td> </tr> <tr> <td colspan="3">${guardian_proof_passport}A- Passport Number</td> <td colspan="6">${guardian_proof_passport_no}</td> <td colspan="3">Passport Expiry Date</td> <td>${guardian_proof_passport_exp}</td> </tr> <tr> <td colspan="3">${guardian_proof_voterid}B- Voter ID Card</td> <td colspan="6">${guardian_proof_voterid_no}</td> <td colspan="4">&nbsp;</td> </tr> <tr> <td colspan="3">${guardian_proof_drivingLicence}D- Driving Licence</td> <td colspan="6">${guardian_proof_drivingLicence_no}</td> <td colspan="3">Driving Licence Expiry Date</td> <td>${guardian_proof_drivingLicence_exp}</td> </tr> <tr> <td colspan="3">${guardian_proof_aadharcard}E- Aadhaar Card</td> <td colspan="6">${guardian_proof_aadharcard_no}</td> <td colspan="4" rowspan="2">&nbsp;</td> </tr> <tr> <td colspan="3">${guardian_proof_nregajobcard}F- NREGA Job Card</td> <td colspan="6">${guardian_proof_nregajobcard_no}</td> </tr> <tr> <td colspan="9">${guardian_proof_otherdoc}Z- Others (any document notified by the central government)</td> <td colspan="3">Identification Number</td> <td>&nbsp;</td> </tr> <tr> <td colspan="13" class="headingsec">7. Remarks (If any)</td> </tr> <tr> <td colspan="13">${remarks1}</td> </tr> <tr> <td colspan="13">${remarks2}</td> </tr> <tr> <td colspan="13" class="headingsec">8. Applicant Declaration</td> </tr> <tr> <td colspan="12"><ul> <li>I hereby declare that the details furnished above are true and correct to the best of my knowledge and belief and I undertake to inform you of any changes therein, immediately. In case any of the above information is found to be false or untrue or misleading or misrepresenting, I am aware that I may be held liable for it. I hereby declare that I am not making this application for the purpose of contravention of any Act, Rules, Regulations or any statute of legislation or any notifications/directions issued by any governmental or statutory authority from time to time.</li> <li>I hereby consent to receiving information from Central KYC Registry through SMS/Email on the above registered number/email address.</li> </ul></td> <td rowspan="2">&nbsp;</td> </tr> <tr> <td colspan="5">Date: ${application_date}</td> <td colspan="7">Place: ${application_place}</td> </tr> <tr> <td colspan="13" class="headingsec">9. Attestation / For Office Use Only</td> </tr> <tr> <td colspan="13">Documents Received CertifiedCopies</td> </tr> <tr> <td rowspan="7">&nbsp; </td> <td colspan="6">&nbsp;</td> <td rowspan="7">&nbsp;</td> <td colspan="5">&nbsp;</td> </tr> <tr> <td colspan="6">KYC Verification Carried Out by (Refer Instruction I)</td> <td colspan="5">Institution Details</td> </tr> <tr> <td colspan="2">Date</td> <td colspan="4">&nbsp;</td> <td colspan="2">Name</td> <td colspan="3">&nbsp;</td> </tr> <tr> <td colspan="2">Emp.Name</td> <td colspan="4">&nbsp;</td> <td colspan="2">Code</td> <td colspan="3">&nbsp;</td> </tr> <tr> <td colspan="2">Emp.Code</td> <td colspan="4">&nbsp;</td> <td colspan="2">Emp.Branch</td> <td colspan="3">&nbsp;</td> </tr> <tr> <td colspan="2">Emp. Designation</td> <td colspan="4">&nbsp;</td> <td colspan="5" rowspan="2">&nbsp;</td> </tr> <tr> <td colspan="6"><p>&nbsp;</p> <p>&nbsp;</p></td> </tr> <tr> <td rowspan="6">&nbsp;</td> <td colspan="6">In-Person Verification (IPV) Carried Out by (Refer Instruction J)</td> <td rowspan="6">&nbsp;</td> <td colspan="5">Institution Details</td> </tr> <tr> <td colspan="2">Date</td> <td colspan="4">&nbsp;</td> <td colspan="2">Name</td> <td colspan="3">&nbsp;</td> </tr> <tr> <td colspan="2">Emp.Name</td> <td colspan="4">&nbsp;</td> <td colspan="2">Code</td> <td colspan="3">&nbsp;</td> </tr> <tr> <td colspan="2">Emp.Code</td> <td colspan="4">&nbsp;</td> <td colspan="2">Emp.Branch</td> <td colspan="3">&nbsp;</td> </tr> <tr> <td colspan="2">Emp. Designation</td> <td colspan="4">&nbsp;</td> <td colspan="5" rowspan="2">&nbsp;</td> </tr> <tr> <td colspan="6"><p>&nbsp;</p> <p>&nbsp;</p></td> </tr> <tr> <td colspan="13">&nbsp;</td> </tr> </tbody> </table> </div> </form></body> </html>`;
            //console.log(html);
            pdf.create(html, options).toFile('businesscard.pdf', function (err, res) {
                if (err) return console.log(err);
                console.log(res); // { filename: '/app/businesscard.pdf' }
            });




        })
        .catch(function (err) {
            // error;
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });

    /*  
       
        


       

        var html = `<!doctype html> <html> <head> <meta charset="utf-8"> <title>Individual KYC Form</title> <style> body{ font-family: "Arial"; font-size:12px; }.headingsec{ background-color: #f4f1f1; }	 table tr{ border:none; } table tr td{ border:none; } .chbwd{ width: 15px; padding:none; margin-right: -4px; } #container{ background: #183884; padding: 10px; }  table{background-color: white;}</style> </head> <body> <div id="container"><form> <table width="100%" border="1"> <tbody> <tr> <td colspan="5" rowspan="3">Know Your Client (KYC)  Application Form (For Individuals only)  (Please fill the form in English and in BLOCK Letters)  Fields marked with '*' are mandatory fields</td> <td width="10%" height="30">Application  </td> <td colspan="7">New ${application_new} </td> </tr> <tr> <td>Type</td> <td colspan="7">Update   KYC Number </td> </tr> <tr> <td>KYC Type</td> <td colspan="7"> ${kyctype_normal}Normal (PAN is mandatory) PAN Exempt Investors (Refer instruction K)</td> </tr> <tr class="headingsec"> <td colspan="13"> 1. Identity Details (Please refer instruction A at the end)</td> </tr> <tr> <td colspan="13"> PAN ${pan_no} Please enclose a duly attested copy of your PAN Card </td> </tr> <tr> <td colspan="3">&nbsp;</td> <td >Prefix</td> <td>First Name</td> <td>Last Name</td> <td colspan="7">Middle Name</td> </tr> <tr> <td colspan="3">Name* (same as ID proof)</td> <td>${applicant_name_prefix}</td> <td>${applicant_firstname}</td> <td>${applicant_lastname}</td> <td colspan="7">${applicant_middlename}</td> </tr> <tr> <td colspan="3">Maiden Name (If any*)</td> <td>${maiden_name_prefix}</td> <td>${maiden_firstname}</td> <td>${maiden_lastname}</td> <td colspan="7">${maiden_middlename}</td> </tr> <tr> <td colspan="3">Father / Spouse Name*</td> <td>${father_name_prefix}</td> <td>${father_firstname}</td> <td>${father_lastname}</td> <td colspan="7">${father_middlename}</td> </tr> <tr> <td colspan="3">Mother Name*</td> <td>${mother_name_prefix}</td> <td>${mother_firstname}</td> <td>${mother_lastname}</td> <td colspan="7">${mother_middlename}</td> </tr> <tr> <td colspan="3">Date of Birth*</td> <td colspan="9">${dob}</td> <td width="22%" rowspan="9">&nbsp;</td> </tr> <tr> <td colspan="3">Gender*</td> <td colspan="3">${gender_male}M- Male</td> <td width="18%" colspan="3">${gender_female}F- Female</td> <td colspan="3">${gender_transgender}T-Transgender</td> </tr> <tr> <td colspan="3">Marital Status*</td> <td colspan="3">${marital_married}Married</td> <td colspan="3">${marital_unmarried} Unmarried</td> <td colspan="3">${marital_others}Others</td> </tr> <tr> <td colspan="3">Citizenship*</td> <td colspan="3">${country_in}IN- Indian </td> <td colspan="3">${othercountry_name}Others – Country</td> <td colspan="3">${othercountry_code} Country Code</td> </tr> <tr> <td colspan="3" rowspan="2">Residential Status*</td> <td colspan="3">${resident_individual}Resident Individual </td> <td colspan="3">${resident_nonindividual}Non Resident Indian</td> <td colspan="3">&nbsp;</td> </tr> <tr> <td colspan="3">${foreign_national}Foreign National</td> <td colspan="3">${person_indianorigin}Person of Indian Origin</td> <td colspan="3">&nbsp;</td> </tr> <tr> <td colspan="3" rowspan="3">Occupation Type*</td> <td colspan="3"> ${occ_service_pri}S-Service Private Sector</td> <td colspan="3"> ${occ_service_pub}Public Sector</td> <td colspan="3"> ${occ_service_gov}Government Sector</td> </tr> <tr> <td colspan="3">${occ_service_others}O-Others Professional</td> <td colspan="3"> ${occ_service_selfemp}Self Employed Retired </td> <td colspan="3">${occ_service_housewife}Housewife Student</td> </tr> <tr> <td colspan="3">${occ_service_business}B-Business</td> <td colspan="3">${occ_service_notcategorised} X-Not Categorised</td> <td colspan="3">&nbsp;</td> </tr> <tr class="headingsec"> <td colspan="13">2. Proof of Identity (PoI)* (for PAN exempt Investor or if PAN card copy not provided) (Please refer instruction C & K at the end)</td> </tr> <tr> <td colspan="13">(Certified copy of any one of the following Proof of Identity [PoI] needs to be submitted)</td> </tr> <tr> <td colspan="3">${proof_passport}A- Passport Number</td> <td colspan="6">${proof_passport_no}</td> <td colspan="3">Passport Expiry Date</td> <td>${proof_passport_exp}</td> </tr> <tr> <td colspan="3">${proof_voterid}B- Voter ID Card</td> <td colspan="6">${proof_voterid_no}</td> <td colspan="4">&nbsp;</td> </tr> <tr> <td colspan="3">${proof_drivingLicence}D- Driving Licence</td> <td colspan="6">${proof_drivingLicence_no}</td> <td colspan="3">Driving Licence Expiry Date</td> <td>${proof_drivingLicence_exp}</td> </tr> <tr> <td colspan="3">${proof_aadharcard}E- Aadhaar Card</td> <td colspan="6">${proof_aadharcard_no}</td> <td colspan="4" rowspan="2">&nbsp;</td> </tr> <tr> <td colspan="3">${proof_nregajobcard}F- NREGA Job Card</td> <td colspan="6">${proof_nregajobcard_no}</td> </tr> <tr> <td colspan="9">${proof_otherdoc}Z- Others (any document notified by the central government)</td> <td colspan="3">Identification Number</td> <td>&nbsp;</td> </tr> <tr> <td colspan="13">3. Proof of Address (PoA)*</td> </tr> <tr> <td colspan="13" class="headingsec">${address_permanent}3.1 Current / Permanent / Overseas Address Details (Please see instruction D at the end)</td> </tr> <tr> <td colspan="13">Address</td> </tr> <tr> <td width="8%" colspan="2">Line 1*</td> <td colspan="11">${address_permanent_1}</td> </tr> <tr> <td colspan="2">Line 2</td> <td colspan="11">${address_permanent_2}</td> </tr> <tr> <td colspan="2">Line 3</td> <td colspan="7">${address_permanent_3}</td> <td colspan="3">City / Town / Village*</td> <td>${address_permanent_city}</td> </tr> <tr> <td colspan="2">District*</td> <td colspan="3">${address_permanent_district}</td> <td colspan="4">Zip / Post Code* ${address_permanent_zipcode}</td> <td colspan="4">State/UT Code ${address_permanent_statecode} as per Indian Motor Vehicle Act, 1988</td> </tr> <tr> <td colspan="2">State/UT*</td> <td colspan="4">${address_permanent_state}</td> <td colspan="5">Country* ${address_permanent_country}</td> <td colspan="2">Country Code ${address_permanent_country_code} as per ISO 3166</td> </tr> <tr> <td colspan="2">Address Type*</td> <td colspan="2">${address_permanent_typeResBusiness} Residential / Business </td> <td colspan="2"> ${address_permanent_typeResidential}Residential </td> <td colspan="4"> ${address_permanent_typeBusiness}Business</td> <td colspan="2"> ${address_permanent_typeRegOffice} Registered Office </td> <td>Unspecified ${address_permanent_typeUnsepecified}</td> </tr> <tr> <td colspan="13">(Certified copy of any one of the following Proof of Address [PoA] needs to be submitted)</td> </tr> <tr> <td colspan="3">${proof_passport_permanent}A- Passport Number</td> <td colspan="6">${proof_passport_no_permanent}</td> <td colspan="3">Passport Expiry Date</td> <td>${proof_passport_exp_permanent}</td> </tr> <tr> <td colspan="3">${proof_voterid_permanent}B- Voter ID Card</td> <td colspan="6">${proof_voterid_no_permanent}</td> <td colspan="4">&nbsp;</td> </tr> <tr> <td colspan="3">${proof_drivingLicence_permanent}D- Driving Licence</td> <td colspan="6">${proof_drivingLicence_no_permanent}</td> <td colspan="3">Driving Licence Expiry Date</td> <td>${proof_drivingLicence_exp_permanent}</td> </tr> <tr> <td colspan="3">${proof_aadharcard_permanent}E- Aadhaar Card</td> <td colspan="6">${proof_aadharcard_no_permanent}</td> <td colspan="4" rowspan="2">&nbsp;</td> </tr> <tr> <td colspan="3">${proof_nregajobcard_permanent}F- NREGA Job Card</td> <td colspan="6">${proof_nregajobcard_no_permanent}</td> </tr> <tr> <td colspan="9">${proof_otherdoc_permanent}Z- Others (any document notified by the central government)</td> <td colspan="3">Identification Number</td> <td>&nbsp;</td> </tr> <tr> <td colspan="13" class="headingsec">${address_localaddress}3.2 Correspondence / Local Address Details* (Please see instruction E at the end)</td> </tr> <tr> <td colspan="13">Same as Current / Permanent / Overseas Address details (In case of multiple correspondence / local addresses, please fill &lsquo;Annexure A1&rsquo;, Submit relevant documentary proof)</td> </tr> <tr> <td width="8%" colspan="2">Line 1*</td> <td colspan="11">${address_localaddress_1}</td> </tr> <tr> <td colspan="2">Line 2</td> <td colspan="11">${address_localaddress_2}</td> </tr> <tr> <td colspan="2">Line 3</td> <td colspan="7">${address_localaddress_3}</td> <td colspan="3">City / Town / Village*</td> <td>${address_localaddress_city}</td> </tr> <tr> <td colspan="2">District*</td> <td colspan="3">${address_localaddress_district}</td> <td colspan="4">Zip / Post Code* ${address_localaddress_zipcode}</td> <td colspan="4">State/UT Code ${address_localaddress_statecode} as per Indian Motor Vehicle Act, 1988</td> </tr> <tr> <td colspan="2">State/UT*</td> <td colspan="4">${address_localaddress_state}</td> <td colspan="5">Country* ${address_localaddress_country}</td> <td colspan="2">Country Code ${address_localaddress_country_code} as per ISO 3166</td> </tr> <tr> <td height="31" colspan="13" class="headingsec">4. Contact Details (All communications will be sent on provided Mobile no. / Email-ID) (Please refer instruction F at the end)</td> </tr> <tr> <td colspan="2">Email ID</td> <td colspan="11">${emailid}</td> </tr> <tr> <td colspan="2">Mobile</td> <td colspan="3">${mobileno}</td> <td colspan="6">Tel. (Off) ${telephone_off}</td> <td colspan="2">Tel. (Res) ${telephone_res}</td> </tr> <tr> <td colspan="13" class="headingsec">5. FATCA/CRS Information (Tick if Applicable) ${fatca_resident} Residence for Tax Purposes in Jurisdiction(s) Outside India (Please refer instruction B at the end)</td> </tr> <tr> <td colspan="13">Additional Details Required* (Mandatory only if above option (5) is ticked)</td> </tr> <tr> <td colspan="4">Country of Jurisdiction of Residence*</td> <td colspan="5">${fatca_country_jurisdiction} </td> <td colspan="4">Country Code of Jurisdiction of Residence as per ${fatca_countrycode_jurisdiction}  ISO 3166</td> </tr> <tr> <td colspan="13">Tax Identification Number or equivalent (If issued by jurisdiction)*</td> </tr> <tr> <td colspan="13">${fatca_taxidentification_no}</td> </tr> <tr> <td colspan="3">Place / City of Birth*</td> <td colspan="3">${fatca_city}</td> <td colspan="5">Country of Birth* ${fatca_country}</td> <td colspan="2">Country Code ${fatca_countrycode}as per ISO 3166</td> </tr> <tr> <td colspan="13">Address</td> </tr> <tr> <td width="8%" colspan="2">Line 1*</td> <td colspan="11">${fatca_address1}</td> </tr> <tr> <td colspan="2">Line 2</td> <td colspan="11">${fatca_address2}</td> </tr> <tr> <td colspan="2">Line 3</td> <td colspan="7">${fatca_address3}</td> <td colspan="3">City / Town / Village*</td> <td>${fatca_city}</td> </tr> <tr> <td colspan="2">District*</td> <td colspan="3">${fatca_district}</td> <td colspan="4">Zip / Post Code* ${fatca_postcalcode}</td> <td colspan="4">State/UT Code ${fatca_statecode}as per Indian Motor Vehicle Act, 1988</td> </tr> <tr> <td colspan="2">State/UT*</td> <td colspan="4">${fatca_state}</td> <td colspan="5">Country* ${fatca_country}</td> <td colspan="2">Country Code  ${fatca_country_code}as per ISO 3166</td> </tr> <tr> <td colspan="13" class="headingsec">6. Details of Related Person (Optional) (please refer instruction G at the end) (in case of additional related persons, please fill &lsquo;Annexure B1&rsquo;)</td> </tr> <tr> <td colspan="4"> ${relatedPerson_relatedPerson}Related Person</td> <td colspan="5"> ${relatedPerson_deletion}Deletion of Related Person</td> <td colspan="4"> ${relatedPerson_kycno}KYC Number of Related Person (if available*)</td> </tr> <tr> <td colspan="4">Related Person Type*</td> <td colspan="2">${relatedPerson_guardian}Guardian of Minor</td> <td colspan="4">${relatedPerson_assignee}Assignee</td> <td colspan="3">${relatedPerson_authorised}Authorized Representative</td> </tr> <tr> <td colspan="3">&nbsp;</td> <td width="7%">Prefix</td> <td width="8%">First Name</td> <td>Last Name</td> <td colspan="7">Middle Name</td> </tr> <tr> <td colspan="3">Name* (same as ID proof)</td> <td>${guardian_prefixname}</td> <td>${guardian_firstname}</td> <td>${guardian_lastname}</td> <td colspan="7">${guardian_middlename}</td> </tr> <tr> <td colspan="3">&nbsp;</td> <td colspan="10">(If KYC number and name are provided, below details of section 6 are optional)</td> </tr> <tr> <td colspan="13">${guardian_proof}Proof of Identity [PoI] of Related Person* (Please see instruction (H) at the end)</td> </tr> <tr> <td colspan="13">(Certified copy of any one of the following Proof of Identity[PoI] needs to be submitted)</td> </tr> <tr> <td colspan="3">${guardian_proof_passport}A- Passport Number</td> <td colspan="6">${guardian_proof_passport_no}</td> <td colspan="3">Passport Expiry Date</td> <td>${guardian_proof_passport_exp}</td> </tr> <tr> <td colspan="3">${guardian_proof_voterid}B- Voter ID Card</td> <td colspan="6">${guardian_proof_voterid_no}</td> <td colspan="4">&nbsp;</td> </tr> <tr> <td colspan="3">${guardian_proof_drivingLicence}D- Driving Licence</td> <td colspan="6">${guardian_proof_drivingLicence_no}</td> <td colspan="3">Driving Licence Expiry Date</td> <td>${guardian_proof_drivingLicence_exp}</td> </tr> <tr> <td colspan="3">${guardian_proof_aadharcard}E- Aadhaar Card</td> <td colspan="6">${guardian_proof_aadharcard_no}</td> <td colspan="4" rowspan="2">&nbsp;</td> </tr> <tr> <td colspan="3">${guardian_proof_nregajobcard}F- NREGA Job Card</td> <td colspan="6">${guardian_proof_nregajobcard_no}</td> </tr> <tr> <td colspan="9">${guardian_proof_otherdoc}Z- Others (any document notified by the central government)</td> <td colspan="3">Identification Number</td> <td>&nbsp;</td> </tr> <tr> <td colspan="13" class="headingsec">7. Remarks (If any)</td> </tr> <tr> <td colspan="13">${remarks1}</td> </tr> <tr> <td colspan="13">${remarks2}</td> </tr> <tr> <td colspan="13" class="headingsec">8. Applicant Declaration</td> </tr> <tr> <td colspan="12"><ul> <li>I hereby declare that the details furnished above are true and correct to the best of my knowledge and belief and I undertake to inform you of any changes therein, immediately. In case any of the above information is found to be false or untrue or misleading or misrepresenting, I am aware that I may be held liable for it. I hereby declare that I am not making this application for the purpose of contravention of any Act, Rules, Regulations or any statute of legislation or any notifications/directions issued by any governmental or statutory authority from time to time.</li> <li>I hereby consent to receiving information from Central KYC Registry through SMS/Email on the above registered number/email address.</li> </ul></td> <td rowspan="2">&nbsp;</td> </tr> <tr> <td colspan="5">Date: ${application_date}</td> <td colspan="7">Place: ${application_place}</td> </tr> <tr> <td colspan="13" class="headingsec">9. Attestation / For Office Use Only</td> </tr> <tr> <td colspan="13">Documents Received CertifiedCopies</td> </tr> <tr> <td rowspan="7">&nbsp; </td> <td colspan="6">&nbsp;</td> <td rowspan="7">&nbsp;</td> <td colspan="5">&nbsp;</td> </tr> <tr> <td colspan="6">KYC Verification Carried Out by (Refer Instruction I)</td> <td colspan="5">Institution Details</td> </tr> <tr> <td colspan="2">Date</td> <td colspan="4">&nbsp;</td> <td colspan="2">Name</td> <td colspan="3">&nbsp;</td> </tr> <tr> <td colspan="2">Emp.Name</td> <td colspan="4">&nbsp;</td> <td colspan="2">Code</td> <td colspan="3">&nbsp;</td> </tr> <tr> <td colspan="2">Emp.Code</td> <td colspan="4">&nbsp;</td> <td colspan="2">Emp.Branch</td> <td colspan="3">&nbsp;</td> </tr> <tr> <td colspan="2">Emp. Designation</td> <td colspan="4">&nbsp;</td> <td colspan="5" rowspan="2">&nbsp;</td> </tr> <tr> <td colspan="6"><p>&nbsp;</p> <p>&nbsp;</p></td> </tr> <tr> <td rowspan="6">&nbsp;</td> <td colspan="6">In-Person Verification (IPV) Carried Out by (Refer Instruction J)</td> <td rowspan="6">&nbsp;</td> <td colspan="5">Institution Details</td> </tr> <tr> <td colspan="2">Date</td> <td colspan="4">&nbsp;</td> <td colspan="2">Name</td> <td colspan="3">&nbsp;</td> </tr> <tr> <td colspan="2">Emp.Name</td> <td colspan="4">&nbsp;</td> <td colspan="2">Code</td> <td colspan="3">&nbsp;</td> </tr> <tr> <td colspan="2">Emp.Code</td> <td colspan="4">&nbsp;</td> <td colspan="2">Emp.Branch</td> <td colspan="3">&nbsp;</td> </tr> <tr> <td colspan="2">Emp. Designation</td> <td colspan="4">&nbsp;</td> <td colspan="5" rowspan="2">&nbsp;</td> </tr> <tr> <td colspan="6"><p>&nbsp;</p> <p>&nbsp;</p></td> </tr> <tr> <td colspan="13">&nbsp;</td> </tr> </tbody> </table> </div> </form></body> </html>`;

        pdf.create(html, options).toFile('businesscard.pdf', function(err, res) {
            if (err) return console.log(err);
            console.log(res); // { filename: '/app/businesscard.pdf' }
          });
     */

});




router.get('/create-pdf-kra-individual', function (req, res, next) {


    var params = req.body;
    var id = "AQNPB9151A";
    var fileName = 'KRA_individual_' + id + '.pdf';
    var sql = "SELECT * FROM kycinfo"
    sql += " WHERE pan=$1"
    db.any(sql, [id])
        .then(function (resultdata) {
            var results = resultdata[0];
            var name =  '<span class="defaultpading">'+results.identityinfo.APP_NAME+'</span>';
            var fathername = '<span class="defaultpading">'+results.identityinfo.APP_F_NAME+' '+results.identityinfo.APP_F_LNAME+'</span>';
            var gender = '<span class="defaultpading">'+results.identityinfo.APP_GEN+'</span>';
            var maritalstatus = '<span class="defaultpading">'+results.identityinfo.APP_MAR_STATUS+'</span>';
            var dob = '<span class="defaultpading">'+results.identityinfo.APP_DOB_DT+'</span>';
            var nationality = (results.identityinfo.APP_NATIONALITY == 01) ? '<span class="defaultpading">India</span>' : '<span class="defaultpading">Other</span>';
            var residentialstatus = '';
            if (results.identityinfo.APP_RES_STATUS == 'R') residentialstatus = '<span class="defaultpading">Residential</span>';
            else if (results.identityinfo.APP_RES_STATUS == 'N') residentialstatus = '<span class="defaultpading">Non Residential</span>';
            else if (results.identityinfo.APP_RES_STATUS == 'P') residentialstatus = '<span class="defaultpading">Foreign National</span>';
            
            var pan = '<span class="defaultpading">'+results.pan+'</span>';
            var aadhar = '<span class="defaultpading"> </span>';
            var proofname_identity = '<span class="defaultpading"> </span>';
            var address = '<span class="defaultpading">'+results.addressinfo.APP_PER_ADD1+' '+results.addressinfo.APP_PER_ADD2+' '+results.addressinfo.APP_PER_ADD3+'</span>';
            var city = '<span class="defaultpading">'+results.addressinfo.APP_PER_CITY+'</span>';
            var pincode = '<span class="defaultpading">'+results.addressinfo.APP_PER_PINCD+'</span>';
            var state = '<span class="defaultpading">'+results.addressinfo.APP_PER_STATE+'</span>';
            var country = '<span class="defaultpading">'+results.addressinfo.APP_PER_CTRY+'</span>';
            var offcontactno = '<span class="defaultpading">'+results.contactinfo.APP_TELEPHONE_NO+'</span>';
            var rescontactno = '';
            var mobileno = '<span class="defaultpading">'+results.contactinfo.APP_MOB_NO+'</span>';
            var fax = '';
            var emailid = '<span class="defaultpading">'+results.contactinfo.APP_EMAIL+'</span>';
            var proofname_residence = '';
            var signature = '';
            var date = ''; 

            var html = `<!doctype html> <html> <head> <meta charset="utf-8"> <title>KYC Application Form</title> <style type="text/css"> .tg  {border-collapse:collapse;border-spacing:0;} .tg td{font-family:Arial, sans-serif;font-size:14px;padding:6px 20px; overflow:hidden;word-break:normal;} .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:6px 20px; overflow:hidden;word-break:normal;} .tg .tg-xldj{border-color:inherit;text-align:left} .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top} .header{text-align: center;font-size: 18px; font-weight: bold;} #containerDiv{ padding: 50px; } .fltright{ text-align: right; } .headerBg{ background-color: #e0e0e0; font-weight: bold; }table tr td{ border:none; } .defaultpading{ font-size: 14px; padding-right: 30px; border-bottom: 1px solid; }</style> </head>  <body> <div id="containerDiv"> <table class="tg" style="width:100%">  <tr> <th class="header" colspan="15" >KNOW YOUR CLIENT (KYC) APPLICATION FORM</th> </tr> <tr> <td class="fltright" colspan="7">For Individuals<br></td> <td class="tg-0pky" colspan="7"></td> </tr> <tr> <td class="tg-0pky headerBg" colspan="7">A. IDENTITY DETAILS</td> <td class="tg-0pky" colspan="7"></td> </tr> <tr> <td class="tg-0pky" colspan="7">1. Name of the Applicant: ${name}</td> <td class="tg-0pky" colspan="7"></td> </tr> <tr> <td class="tg-0pky" colspan="7">2.Father’s/ Spouse Name: ${fathername}</td> <td class="tg-0pky" colspan="7"></td>   </tr> <tr> <td class="tg-0pky" colspan="15">3. a. Gender: ${gender} b. Marital status:  ${maritalstatus} c. Date of birth:  ${dob}(dd/mm/yyyy)</td> </tr> <tr> <td class="tg-0pky" colspan="15">4. a. Nationality:  ${nationality} b. Status: ${residentialstatus}</td> </tr> <tr> <td class="tg-0pky" colspan="15">5. a. PAN: ${pan} b. Aadhaar Number, if any: ${aadhar}</td> </tr> <tr> <td class="tg-0pky" colspan="15">6. Specify the proof of Identity submitted: ${proofname_identity}</td> </tr> <tr> <td class="tg-0pky headerBg" colspan="15">B. ADDRESS DETAILS</td> </tr> <tr> <td class="tg-0pky" colspan="15">1. Residence Address: ${address}</td> </tr> <tr> <td class="tg-0pky" colspan="15"> City/town/village: ${city} Pin Code: ${pincode} State: ${state} Country: ${country}</td> </tr> <tr> <td class="tg-0pky" colspan="15">2. Contact Details: Tel. (Off.) ${offcontactno} Tel. (Res.) ${rescontactno} Mobile No.: ${mobileno} Fax: ${fax} Email id: ${emailid}</td> </tr> <tr> <td class="tg-0pky" colspan="15">3. Specify the proof of address submitted for residence address:${proofname_residence}</td> </tr> <tr> <td class="tg-0pky" colspan="15">4. Permanent Address (if different from above or overseas address, mandatory for Non-Resident Applicant): ____________</td> </tr> <tr> <td class="tg-0pky" colspan="15">________ City/town/village: ___________ Pin Code: _________ State: ______________ Country: __________________</td> </tr> <tr> <td class="tg-0pky headerBg" colspan="15">DECLARATION</td> </tr> <tr> <td class="tg-0pky" colspan="15">I hereby declare that the details furnished above are true and correct to the best of my knowledge and belief and I undertake to<br>inform you of any changes therein, immediately. In case any of the above information is found to be false or untrue or<br>misleading or misrepresenting, I am aware that I may be held liable for it.</td> </tr> <tr> <td class="tg-0pky" colspan="15">${signature}</td> </tr> <tr> <td class="tg-0pky" colspan="15">Signature of the Applicant Date: ${date} (dd/mm/yyyy)</td> </tr> <tr> <td class="tg-0pky" colspan="15">FOR OFFICE USE ONLY</td> </tr> <tr> <td class="tg-0pky" colspan="15">Originals verified and Self-Attested Document copies received</td> </tr> <tr> <td class="tg-0pky" colspan="15">(………………………………………..)<br>Name &amp; Signature of the Authorised Signatory</td> </tr> <tr> <td class="tg-0pky" colspan="">Date …………………. </td> <td class="tg-0pky" colspan=""> Seal/Stamp of the intermediary</td> </tr> </table> </div> </body> </html> `;
            //console.log(html);
            pdf.create(html, options).toFile(fileName, function (err, res) {
                if (err) return console.log(err);
                console.log(res); // { filename: '/app/businesscard.pdf' }
            });
        })
        .catch(function (err) {
            // error;
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });


});



function buildPdfInput(argText, argLenght, argClass) {
    var buildList = '',
        i = '';
    for (i = 0; i < argLenght; i++) {
        buildList += '<input type="text" value="' + argText.charAt(i) + '" class="' + argClass + '">';
    }
    return buildList;
}

function buildPdfInputDate(arg, argClass) {
    var buildList = '';
    if (arg) {
        buildList += '<input type="text"  class="' + argClass + '" value=' + arg.charAt(8) + '>';
        buildList += '<input type="text"  class="' + argClass + '" value=' + arg.charAt(9) + '> - ';
        buildList += '<input type="text"  class="' + argClass + '" value=' + arg.charAt(5) + '>';
        buildList += '<input type="text"  class="' + argClass + '" value=' + arg.charAt(6) + '> - ';
        buildList += '<input type="text"  class="' + argClass + '" value=' + arg.charAt(0) + '>';
        buildList += '<input type="text"  class="' + argClass + '" value=' + arg.charAt(1) + '>';
        buildList += '<input type="text"  class="' + argClass + '" value=' + arg.charAt(2) + '>';
        buildList += '<input type="text"  class="' + argClass + '" value=' + arg.charAt(3) + '>';
    } else {
        buildList += '<input type="text"  class="' + argClass + '" value=' + arg.charAt(8) + '>';
        buildList += '<input type="text"  class="' + argClass + '" value=' + arg.charAt(9) + '> - ';
        buildList += '<input type="text"  class="' + argClass + '" value=' + arg.charAt(5) + '>';
        buildList += '<input type="text"  class="' + argClass + '" value=' + arg.charAt(6) + '> - ';
        buildList += '<input type="text"  class="' + argClass + '" value=' + arg.charAt(0) + '>';
        buildList += '<input type="text"  class="' + argClass + '" value=' + arg.charAt(1) + '>';
        buildList += '<input type="text"  class="' + argClass + '" value=' + arg.charAt(2) + '>';
        buildList += '<input type="text"  class="' + argClass + '" value=' + arg.charAt(3) + '>';
    }
    return buildList;
}

function buildCheckBox(arg, argClass) {
    var buildList = '';
    if (arg) {
        buildList = '<input type="text"  class="' + argClass + '" checked>';
    } else {
        buildList = '<input type="text"  class="' + argClass + '">';
    }
    return buildList;
}





module.exports = router;