var express = require('express');
var router = express.Router();
var db = require('./dbconnections_controller');
var util = require('./../config/helper');

//::LIST
router.get('/', function (req, res) {
   
    var params = req.body;
    var offset = util.IsNull(req.query.offset, 0);
    var limit = util.IsNull(req.query.limit, 10);
    var orderby = util.IsNull(req.query.orderby);
    var sortby = util.IsNull(req.query.sortby);
    var status = util.IsNull(req.query.status);
    var ticketid = util.IsNull(req.query.ticketid);
    var cityid = util.IsNull(req.query.cityid);
    var assignedto = util.IsNull(req.query.assignedto);


    var sql = "SELECT t.*,u.username,u.postalcode,u.address,u.email,u.mobileno,cl.city_name as cityname,cc.centername FROM ticket as t LEFT JOIN users as  u  ON t.addedby = u.userid LEFT JOIN citylist as cl ON t.city_id = cl.city_id ";
    sql += " LEFT JOIN collectioncenter as cc ON t.centerid = cc.centerid where true "; 
    if (status != null && status != "") {
        // sql += " AND t.ticketstatus IN ($3) "; 
        sql += " AND t.ticketstatus=any($3::int[]) "; 
        status = status.split(',');
    }
    if(ticketid != null && ticketid >0){
        sql += " AND t.ticketid= $4 "; 
    }
    if(cityid != null && cityid >0){
        sql += "  AND t.city_id= $5 "; 
    }
    if(assignedto != null && assignedto >0){
        sql += " AND t.assignedto= $6 "; 
    }
    if (orderby != null && orderby != "") {
        sql += " order by " + orderby;
        if (sortby != null && sortby != "") {
            sql += " " + sortby;
        }
    }

    sql += " offset $1 limit $2 ";
   // console.log('ticketlist', sql, offset, limit,ticketid);
   //status.split(',').map(function(el){return parseInt(el)})
    db.any(sql, [offset, limit,status,ticketid,cityid,assignedto])
        .then(function (results) {
            res.status(200);
            res.send(results);
        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });

});


//::GETITEM
router.get('/:id', function (req, res) {
    var params = req.body;
    var id = req.params.id

    var sql = "SELECT t.*,u.username FROM ticket as t LEFT JOIN users as  u  ON t.userid = u.userid where "
    sql += " t.ticketid=$1"
    db.any(sql, [id])
        .then(function (results) {
            var result = (results.length > 0) ? results[0] : {}
            res.status(200);
            res.send(result);
        })
        .catch(function (err) {
            // error;
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });
});

//::ADD
router.post('/', function (req, res) {
    var params = req.body;
    var userid = util.IsNull(params.userid); 
    var ticketstatus = 1; 
    var addedby = util.IsNull(params.addedby);  
    var centerid = util.IsNull(params.centerid);  
    var cityid = util.IsNull(params.cityid); 
    var donarcomment = util.IsNull(params.donarcomment); 
    var timeofcontact = util.IsNull(params.timeofcontact); 
    var authenticationcode = Math.floor(1000 + Math.random() * 9000);

    var sql = "INSERT INTO ticket(userid, ticketstatus, addedby,centerid,city_id,donarcomment,timeofcontact,authenticationcode)"
    sql += " VALUES($1,$2,$3,$4,$5,$6,$7,$8)  RETURNING ticketid";
    
    db.any(sql, [userid, ticketstatus,addedby,centerid,cityid,donarcomment,timeofcontact,authenticationcode
        ])
        .then(function (results) {
            res.status(200);
            res.send({
                message: "Created Successfully",
                id: results[0].ticketid,
                    authenticationcode:authenticationcode
            });
        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });
});

//::UPDATE
router.post('/:id', function (req, res) {
    var params = req.body;

    var id = req.params.id
    var ticketstatus = (params.ticketstatus); 
    var updatedby = util.IsNull(params.updatedby);
    var cityid = util.IsNull(params.cityid);
    var assignedTo = util.IsNull(params.assignedTo); 
    var dropboxid = util.IsNull(params.dropboxid);  
    var medicinedroparea= util.IsNull(params.medicinedroparea); 
    var sql = "UPDATE ticket SET ticketstatus =$2, updatedby=coalesce($3,updatedby), city_id=coalesce($4,city_id), assignedto =coalesce($5,assignedto),dropboxid =coalesce($6,dropboxid),medicinedroparea =coalesce($7,medicinedroparea)  ";
    sql += " WHERE ticketid=$1";
  // console.log('Update ticket :: ',sql);
  // console.log('Update ticket input :: ',id, ticketstatus,updatedby,cityid,assignedTo,dropboxid,medicinedroparea);

    db.any(sql, [id, ticketstatus,updatedby,cityid,assignedTo,dropboxid,medicinedroparea
        ])
        .then(function (results) {
            res.status(200);
            res.send({
                status:201,
                message: "updated successfully",
                id: id
            });
        })
        .catch(function (err) {
            console.log('medicine update :: ',err);
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });
});
 

module.exports = router;