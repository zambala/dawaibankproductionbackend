var express = require('express');
var router = express.Router();
var db = require('./dbconnections_controller');
var util = require('./../config/helper');
//var tiggerEmail = require('./../controller/email_controller');
var bcrypt = require('bcrypt-nodejs');
var utilCtrl = require('./utility');
var jwt = require('jsonwebtoken');


//::LOGIN
router.post('/login', function (req, res) {
    var params = req.body;
    var email = util.IsNull(params.email);
    var password = util.IsNull(params.password);
    var userstatus = '1';

    var sql = "SELECT userid, username,password, email, mobileno, postalcode, userrole, userstatus,centerid,address,fullname FROM users WHERE username=$1 AND userstatus =$3 ";
    db.any(sql, [email, password, userstatus])
        .then(function (results) {
           // console.log('Loging response', results);
            if (results != null && results.length > 0) {

                var dbhash = results[0].password;
                bcrypt.compare(password, dbhash, function (err, resp) {

                    if (resp) { 
                        delete results[0].password;
                       var claims = {   sub: 'aid2needy', iss: 'https://aid2needy.com',  permissions: 'authenticated' ,userid:results[0].userid }
                       var token = jwt.sign(claims,util.jwt_seceret_key); //app.get('apisecretkey'));
                       results[0].accesstoken = token;
                       
                        res.status(201);
                        res.send({
                            message: "success",
                            status: 201,
                            data: results
                            // data: results
                        });

                    } else {
                        res.status(401);
                        res.send({
                            message: "User Id/Password is Invalid",
                            status: 401
                        });
                    }

                });

            } else {
                res.status(401);
                res.send({
                    message: "User Id/Password is Invalid",
                    status: 401
                });

            }
        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });
});


//::LIST
router.get('/', function (req, res) {

    /*  const emailoptions = {
        url: req.protocol + '://' + req.headers.host + '/api/sendmail/email',
        method: 'POST',
        body: '',
        json: true,
        headers: {
            'Accept': 'application/json',
            'Accept-Charset': 'utf-8',
            'User-Agent': 'my-reddit-client'
        }
    };

    var emailJsonObj = {
        'email': 'chidambaram1987@gmail.com', //req.body.email,
        'mailtype': 'newuser'
    };
    emailoptions.body = emailJsonObj;
    request(emailoptions, function (err, res, body) {
        console.log('Send email requrest triggered ::', body);
    });

 */

    var params = req.body;
    var offset = util.IsNull(req.query.offset, 0);
    var limit = util.IsNull(req.query.limit, 10);
    var orderby = util.IsNull(req.query.orderby);
    var sortby = util.IsNull(req.query.sortby);
    var status = util.IsNull(req.query.status);
    var searchString = util.IsNull(req.query.searchString);
    var searchType = util.IsNull(req.query.searchType);
    var userrole = util.IsNull(req.query.userrole);
    var cityid = util.IsNull(req.query.cityid);

    var sql = "SELECT userid, username, email, mobileno, address, postalcode, userrole, contactmethod, timeofcontact, userstatus,comments,centerid,fullname,city_name FROM users left join citylist as c ON (address::json->>'city')::integer = c.city_id  where TRUE  ";

    if (searchString != null && searchType != null) {
        sql += " AND " + searchType + " ILIKE '%" + searchString + "%' ";
    }
    if (userrole != null) {
        sql += " AND userrole IN (" + userrole.split(',').join(',') + ") ";
    }
    if (status != null) {
        sql += " AND userstatus = $4";
    }
    if (cityid !='' && cityid != null) {

        sql += " AND (address::json->>'city')::integer = $5 ";
    }


    if (orderby != null && orderby != "") {
        sql += " order by " + orderby;
        if (sortby != null && sortby != "") {
            sql += " " + sortby;
        }
    }
    sql += " offset $1 limit $2 ";
     console.log('userlist', sql);
     console.log('userlist input :: ', offset, limit, userrole, status, cityid );
    db.any(sql, [offset, limit, userrole, status, cityid])
        .then(function (results) {
            res.status(200);
            res.send(
                /* {
                message: "users",
                id: results[0].currval
            } */
                results
            );
        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });

});


//::GETITEM
router.get('/:id', function (req, res) {
    var params = req.body;
    var id = req.params.id

    var sql = "SELECT * FROM USERS"
    sql += " WHERE userid=$1"
    db.any(sql, [id])
        .then(function (results) {
            var result = (results.length > 0) ? results[0] : {}
            res.status(200);
            res.send(result);
        })
        .catch(function (err) {
            // error;
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });
});

//::ADD
/*
mandatory fields
username , eamil, role
*/
router.post('/', function (req, res) {
    var params = req.body;

    var fullname = util.IsNull(params.fullname);
    var username = util.IsNull(params.username);
    var password = util.IsNull(params.password);
    var email = util.IsNull(params.email);
    var mobileno = util.IsNull(params.mobileno);
    var address = util.IsNull(params.address);
    var postalcode = util.IsNull(params.postalcode);
    var userrole = util.IsNull(params.userrole);
    var contactmethod = util.IsNull(params.contactmethod);
    var timeofcontact = util.IsNull(params.timeofcontact);
    var userstatus = util.IsNull(params.userstatus,'1');
    var addedby = util.IsNull(params.addedby);
    var centerid = util.IsNull(params.centerid);
    var sendemail = util.IsNull(params.sendemail);

    var salt = bcrypt.genSaltSync(10);
    var encryptedpassword = bcrypt.hashSync(password, salt);
    //var salt = bcrypt.genSaltSync(10);



    var sqlCheckUsername = "SELECT * FROM users WHERE username=$1";
    db.any(sqlCheckUsername, [username])
        .then(function (results) {

            if (results != null && results.length == 0) {
                var sql = "INSERT INTO users(username, password, email, mobileno, address, postalcode,"
                sql += " userrole, contactmethod, timeofcontact, userstatus, addedby,centerid,fullname,salt) "
                sql += " VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14) RETURNING userid";
                // console.log('Insert New user', sql);
                db.any(sql, [username, encryptedpassword, email, mobileno, address, postalcode, userrole, contactmethod,
                        timeofcontact, userstatus, addedby, centerid, fullname, salt
                    ])
                    .then(function (results) {

                        if (sendemail && userrole == 3) {
                            var emailObj = {
                                name: fullname,
                                email: email,
                                type: 'newvolunter'
                            }
                            utilCtrl.sendEmail(emailObj, function (success) {
                                console.log('mail delivered from add user: ' + success);
                            });
                            var smsmessage = 'We are glad to welcome you as a volunteer for aid2needy team. Thank you for registering with us';
                            utilCtrl.sendSMS(smsmessage, mobileno, function (success) {
                                console.log('SMS  newvolunter: ' + success);
                            });
                        }
                        res.status(200);
                        res.send({
                            status: 201,
                            message: "User created",
                            userid: results[0].userid
                        });
                    })
                    .catch(function (err) {
                        console.log('ERROR USER CREATION', err);
                        var code = err.code == 23505 ? 409 : 400
                        res.status(code);
                        res.send(err);
                    });

            } else {
                res.status(200);
                res.send({
                    message: "Username is already exist",
                    userid: results[0].userid
                });
            }
        })
        .catch(function (err) {
            console.log('user insert error ::', err);
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });
});

//::UPDATE
router.post('/:id', function (req, res) {
    var params = req.body;

    var id = req.params.id
    var username = util.IsNull(params.username);
    var password = util.IsNull(params.password);
    var email = util.IsNull(params.email);
    var mobileno = util.IsNull(params.mobileno);
    var address = util.IsNull(params.address);
    var postalcode = util.IsNull(params.postalcode);
    var userrole = util.IsNull(params.userrole);
    var contactmethod = util.IsNull(params.contactmethod);
    var timeofcontact = util.IsNull(params.timeofcontact);
    var userstatus = util.IsNull(params.userstatus);
    var updatedby = util.IsNull(params.updatedby);
    var centerid = util.IsNull(params.centerid);
    var fullname = util.IsNull(params.fullname);
    var sendEmail = util.IsNull(params.sendmail);

    var sqluser = "select * from users where userid=$1";
    db.any(sqluser, [id]).then(function (userresults) {
        if (userresults && userresults.length > 0) {
            var salt = userresults[0].salt;
            var encryptedpassword = bcrypt.hashSync(password, salt);

            var sql = "UPDATE users SET  userrole=coalesce($2,userrole),userstatus=coalesce($3,userstatus),updatedby=coalesce($4,updatedby),centerid=coalesce($5,centerid),fullname=coalesce($6,fullname) ,username=coalesce($8,username),address=coalesce($9,address)";
            if (password != '' && password != null) {
                sql += " ,password=coalesce($7,password) ";
            }
            sql += " WHERE userid=$1";

            db.any(sql, [id, userrole, userstatus, updatedby, centerid, fullname, encryptedpassword, username,address])
                .then(function (results) {
                    if (sendEmail == 1) {
                        var emailObj = {
                            email: email,
                            newpassword: password,
                            username: username,
                            type: 'admin-centerrequest-approved',
                            name: fullname,
                            userrole: userrole
                        }
                        utilCtrl.sendEmail(emailObj, function (success) {
                            console.log('mail delivered approve user request: ' + success);
                        });
                        var smsmessage = 'We are glad to welcome you as a admin for aid2needy team. Thank you for regsitering with us';


                        utilCtrl.sendSMS(smsmessage, mobileno, function (success) {
                            console.log('SMS  approve user request: ' + success);
                        });


                    }
                    res.status(200);
                    res.send({
                        status: 201,
                        message: "User update",
                        id: id
                    });
                })
                .catch(function (err) {
                    console.log('ERROR UPDATE USER');
                    var code = err.code == 23505 ? 409 : 400
                    res.status(code);
                    res.send(err);
                });
        } else {

            console.log('ERROR UPDATE USER 1'.err);
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);

        }

    }).catch(function (err) {
        console.log('ERROR UPDATE USER 1'.err);
        var code = err.code == 23505 ? 409 : 400
        res.status(code);
        res.send(err);
    })

    /* 
    var salt = bcrypt.genSaltSync(10);
        var encryptedpassword = '';
        if(password!=''&&password!=null){encryptedpassword = bcrypt.hashSync(password, salt);}


        var sql = "UPDATE users SET username =coalesce($2,username),password =coalesce($3,password),email=coalesce($4,email),";
        sql += "mobileno=coalesce($5,mobileno),address=coalesce($6,address),postalcode=coalesce($7,postalcode),";
        sql += "userrole=coalesce($8,userrole),contactmethod=coalesce($9,contactmethod),";
        sql += "timeofcontact=coalesce($10,timeofcontact),userstatus=coalesce($11,userstatus),updatedby=coalesce($12,updatedby),centerid=coalesce($13,centerid),fullname=coalesce($14,fullname) ";
        sql += " WHERE userid=$1";

        db.any(sql, [id, username, encryptedpassword, email, mobileno, address, postalcode, userrole, contactmethod,
                timeofcontact, userstatus, updatedby, centerid,fullname
            ])
            .then(function (results) {
                res.status(200);
                res.send({
                    status: 201,
                    message: "User update",
                    id: id
                });
            })
            .catch(function (err) {
                var code = err.code == 23505 ? 409 : 400
                res.status(code);
                res.send(err);
            });
     */


});

//::LIST
router.get('/donar/details', function (req, res) {


    var userid = util.IsNull(req.query.donarId);
    var sql = "SELECT username, email, mobileno, address, postalcode FROM users where userid=$1 ";

    db.any(sql, [userid])
        .then(function (results) {
            res.status(200);
            res.send(results);
        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });

});

//::LIST
router.get('/ticketassigned/details', function (req, res) {


    var ticketid = util.IsNull(req.query.ticketid);
    var sqlticket = "select assignedto from ticket where ticketid=$1";
    db.any(sqlticket, [ticketid])
        .then(function (results) {
            let userid = results[0].assignedto;
            var sql = "SELECT username, email, mobileno, address, postalcode FROM users where userid=$1 ";
            db.any(sql, [userid])
                .then(function (results) {
                    res.status(200);
                    res.send(results);
                })
                .catch(function (err) {
                    var code = err.code == 23505 ? 409 : 400
                    res.status(code);
                    res.send(err);
                });



        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });



});


//::FORGET PASSWORD
router.post('/forgetpassword/user', function (req, res) {

    var params = req.body;
    var username = util.IsNull(params.username);
    var email = util.IsNull(params.email);

    var sqluser = "select username,fullname from users where username=$1 AND email=$2 and userstatus='1' ";
    db.any(sqluser, [username, email])
        .then(function (sqluserresults) {
            if (sqluserresults && sqluserresults.length > 0) {

                var password = Math.random().toString(36).substring(7);
                var salt = bcrypt.genSaltSync(10);
                var encryptedpassword = bcrypt.hashSync(password, salt);

                var sql = "update users set password=$2, salt=$3 WHERE username=$1";

                db.any(sql, [username, encryptedpassword, salt])
                    .then(function (results) {
                        var emailObj = {
                            name: sqluserresults[0].fullname,
                            email: email,
                            newpassword: password,
                            type: 'forgetPassword'
                        }
                        utilCtrl.sendEmail(emailObj, function (success) {
                            console.log('mail delivered from add user: ' + success);
                        });

                        res.status(200);
                        res.send({
                            status: 200,
                            message: "New password has been sent to your registered email."
                        });
                    })
                    .catch(function (err) {
                        var code = err.code == 23505 ? 409 : 400
                        res.status(code);
                        res.send(err);
                    });
            } else {
                res.send({
                    status: 409,
                    message: "Username and email is not matched"
                });

            }

        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });


});

router.post('/add/newrequest', function (req, res) {

    var params = req.body;
    var fullname = util.IsNull(params.fullname);
    var email = util.IsNull(params.email);
    var mobileno = util.IsNull(params.mobileno);
    var address = util.IsNull(params.address);
    var postalcode = util.IsNull(params.postalcode);
    var userrole = util.IsNull(params.userrole);
    var contactmethod = util.IsNull(params.contactmethod);
    var timeofcontact = util.IsNull(params.timeofcontact);
    var userstatus = util.IsNull(params.userstatus);
    var addedby = util.IsNull(params.addedby);
    var centerid = util.IsNull(params.centerid);
    var sendemail = util.IsNull(params.sendemail);

    var sql = "INSERT INTO users(email, mobileno, address, postalcode,"
    sql += " userrole, contactmethod, timeofcontact, userstatus, addedby,centerid,fullname) "
    sql += " VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11) RETURNING userid";
    // console.log('Insert New user', sql);
    db.any(sql, [email, mobileno, address, postalcode, userrole, contactmethod,
            timeofcontact, userstatus, addedby, centerid, fullname
        ])
        .then(function (results) {

            if (sendemail) {
                var emailObj = {
                    name: fullname,
                    email: email,
                    type: 'newrequest'
                }
                utilCtrl.sendEmail(emailObj, function (success) {
                    console.log('mail delivered from add user: ' + success);
                });
            }


            res.status(200);
            res.send({
                status: 201,
                message: "Request Submitted",
                userid: results[0].userid
            });
        })
        .catch(function (err) {
            console.log('ERROR createnewrequest', err);
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });

});



module.exports = router;