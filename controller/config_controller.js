var express = require('express');
var router = express.Router();
var db = require('./dbconnections_controller');
var util = require('./../config/helper');



//::LIST
router.get('/citylistbycountry', function (req, res) {

  //  console.log('citylistbycountry');

    var countryid = util.IsNull(req.query.countryid, 101);
   // var sql = "SELECT id from statelist  where country_id IN($1) "; //AND status=TRUE "; 
    var sql = "select * from countrylist where status=TRUE and countryid=$1"; 

    db.any(sql, [countryid])
        .then(function (results) {
            // var stateIdList = results.map(function (stateid) {
            //     return stateid.id.toString();
            // });
          //  var sqlcity = "SELECT  * from citylist  where countryid IN("+stateIdList +") order by city_name asc "; //and   status=TRUE ";

            var countrycode = results[0].countrycode;
            var sqlcity = "SELECT  * from citylist  where countryid IN($1) and status=TRUE order by city_name asc ";

           // console.log(sqlcity);
            db.any(sqlcity,[countrycode]).then(function (resultscity) {
                res.status(200);
                res.send(resultscity);
            }).catch(function (err) {
                console.log('citylist error', err);
                var code = err.code == 23505 ? 409 : 400
                res.status(code);
                res.send(err);
            });
        })
        .catch(function (err) {
            console.log(err);
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });

});

router.get('/citylistTempNotUseNow', function (req, res) {

    
      //  we modifed the coutry id to country code, for tempory fix

      var countryid = util.IsNull(req.query.countryid, 101);
      var sql = "select * from countrylist where status=TRUE and countryid=$1"; 
      
  
      db.any(sql, [countryid])
          .then(function (results) {
               var countrycode = results[0].countrycode;
              var sqlcity = "SELECT  * from citylist  where countryid IN($1) and status=TRUE order by city_name asc ";
             // console.log(sqlcity);
              db.any(sqlcity,[countrycode]).then(function (resultscity) {
                  res.status(200);
                  res.send(resultscity);
              }).catch(function (err) {
                  console.log('citylist error', err);
                  var code = err.code == 23505 ? 409 : 400
                  res.status(code);
                  res.send(err);
              });
          })
          .catch(function (err) {
              console.log(err);
              var code = err.code == 23505 ? 409 : 400
              res.status(code);
              res.send(err);
          });
           
 
 });
 router.get('/citylist', function (req, res) {

    var countryid = util.IsNull(req.query.countryid, 101);
       var sql = "SELECT id from statelist  where country_id IN($1) AND status=TRUE ";  
    db.any(sql, [countryid])
        .then(function (results) {
            var stateIdList = results.map(function (stateid) {
                return stateid.id.toString();
            });
           // console.log(stateIdList);
            var sqlcity = "SELECT  * from citylist  where state IN("+stateIdList +") and status=TRUE order by city_name asc ";
           // console.log(sqlcity);
            db.any(sqlcity).then(function (resultscity) {
                res.status(200);
                res.send(resultscity);
            }).catch(function (err) {
                console.log('citylist error', err);
                var code = err.code == 23505 ? 409 : 400
                res.status(code);
                res.send(err);
            });
        })
        .catch(function (err) {
            console.log(err);
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });
         

});
//::LIST
router.get('/citylistbeforehacking', function (req, res) {

   // After hacking we modifed the coutry id to country code, for tempory fix

       var countryid = util.IsNull(req.query.countryid, 101);
       var sql = "SELECT id from statelist  where country_id IN($1) AND status=TRUE "; 
       
   
       db.any(sql, [countryid])
           .then(function (results) {
               var stateIdList = results.map(function (stateid) {
                   return stateid.id.toString();
               });
              // console.log(stateIdList);
               var sqlcity = "SELECT  * from citylist  where state IN("+stateIdList +") and status=TRUE order by city_name asc ";
              // console.log(sqlcity);
               db.any(sqlcity).then(function (resultscity) {
                   res.status(200);
                   res.send(resultscity);
               }).catch(function (err) {
                   console.log('citylist error', err);
                   var code = err.code == 23505 ? 409 : 400
                   res.status(code);
                   res.send(err);
               });
           })
           .catch(function (err) {
               console.log(err);
               var code = err.code == 23505 ? 409 : 400
               res.status(code);
               res.send(err);
           });
   

});

//::LIST
router.get('/countrylist', function (req, res) {

    var sql = "SELECT  * from countrylist  where status=TRUE order by countryname asc";
    console.log('countrylist ',sql);
    db.any(sql)
        .then(function (results) {
            res.status(200);
            res.send(results);
        })
        .then(null,function (err) {
            console.log('ERROR Countrylist',err);
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });

});

//::LIST for admin to activate / inactivate 
router.get('/getaallcountrylist', function (req, res) {

    var sql = "SELECT  * from countrylist order by countryname asc ";
    db.any(sql)
        .then(function (results) {
            res.status(200);
            res.send(results);
        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });

});

//::LIST for admin to activate / inactivate 
router.get('/getaallstatelist', function (req, res) {

    var countryid = util.IsNull(req.query.countryid, 101);
    var sql = "SELECT id,name from statelist  where country_id IN($1) order by name asc"; //AND status=TRUE "; 
    db.any(sql, [countryid]) 
        .then(function (results) {
            res.status(200);
            res.send(results);
         })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });

});
//::LIST for admin to activate / inactivate 
router.get('/getaallcitylist', function (req, res) {

    var stateid = util.IsNull(req.query.stateid, 101);

   // var sqlcity = "SELECT  * from citylist  where state IN($1) ";
    var sqlcity = "SELECT  * from citylist  where countryid IN($1)  order by city_name asc ";
    // console.log(sqlcity);
     db.any(sqlcity, [stateid]).then(function (resultscity) {
         res.status(200);
         res.send(resultscity);
     }).catch(function (err) {
         console.log('citylist error', err);
         var code = err.code == 23505 ? 409 : 400
         res.status(code);
         res.send(err);
     });

});
 



module.exports = router;