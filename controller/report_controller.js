var express = require('express');
var router = express.Router();
var db = require('./dbconnections_controller');
var util = require('./../config/helper');

//::medicine delivery to patient
router.get('/medicinedelivertopatient', function (req, res) {


    var offset = util.IsNull(req.query.offset, 0);
    var limit = util.IsNull(req.query.limit, 10);
    //  var orderby = util.IsNull(req.query.orderby);
    //  var sortby = util.IsNull(req.query.sortby);
    var fromdate = util.IsNull(req.query.fromdate);
    var todate = util.IsNull(req.query.todate);
    var cityid = util.IsNull(req.query.cityid);
    var centerid = util.IsNull(req.query.centerid);

    var sql = "select ml.receiverid,count(ml.quantity) as quantity,p.patientname,p.mobileno,p.email from medicinelog as ml inner join patient as p on ml.receiverid =  p.patientid  where ml.dispatchedon::date between $3 AND $4 ";
    if (cityid != null && cityid != "") {
        sql += " AND ml.cityid=$5 ";
    }
    if (centerid != null && centerid != "") {
        sql += " AND ml.centerid=$6 ";
    }
    sql += " group by ml.receiverid, ml.dispatchedon,p.patientname,p.mobileno,p.email order by ml.dispatchedon desc  ";
    sql += " offset $1 limit $2 ";

    db.any(sql, [offset, limit, fromdate, todate, cityid, centerid])
        .then(function (results) {
            res.status(200);
            res.send(results);
        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });

});

router.get('/getallPatientmedicine', function (req, res) {

    var patientid = util.IsNull(req.query.patientid);
    var centerid = util.IsNull(req.query.centerid);
    var fromdate = util.IsNull(req.query.fromdate);
    var todate = util.IsNull(req.query.todate);

    var sql = "select ml.medicineid,ml.quantity,m.medicinename,m.companyname,m.medicinetype from medicinelog as ml left join medicine as m on ml.medicineid = m.medicineid where ml.receiverid =$1 and ml.centerid=$2 and ml.dispatchedon::date between $3 AND $4 order by ml.dispatchedon desc ";

    db.any(sql, [patientid, centerid, fromdate, todate])
        .then(function (results) {
            res.status(200);
            res.send(results);
        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });

});

router.get('/getdashbordcount', function (req, res) {

    var fromdate = util.IsNull(req.query.fromdate);
    var todate = util.IsNull(req.query.todate);

    var sql = "select (select count(*) from collectioncenter  where centerstatus=1) as collectioncentercount, (select count(*) from dropbox  where status=1) as dropboxcount, (select count(*) from users  where userrole=3) as volunteercount,(select count(*) from ticket where ticketstatus=1) as openticketcount ";
	db.any(sql)
        .then(function (results) {
            res.status(200);
            res.send(results[0]);
        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });
});




module.exports = router;