var express = require('express');
var router = express.Router();
var db = require('./dbconnections_controller');
var util = require('./../config/helper');

//::LIST
    router.get('/', function (req, res) {

    var params = req.body;
    var offset = util.IsNull(req.query.offset, 0);
    var limit = util.IsNull(req.query.limit, 10);
    var orderby = util.IsNull(req.query.orderby);
    var sortby = util.IsNull(req.query.sortby);
    var status = util.IsNull(req.query.status);    
    var searchString = util.IsNull(req.query.searchString);
    var searchType = util.IsNull(req.query.searchType);
    var centerid = util.IsNull(req.query.centerid);
    var ticketid = util.IsNull(req.query.ticketid);
    var cityid = util.IsNull(req.query.cityid);
    var producttype = util.IsNull(req.query.producttype,1);

     

    var sql = "SELECT m.*, cc.centername,c.city_name  FROM medicine as m left join collectioncenter as cc ON m.centerid = cc.centerid  left join citylist as c ON m.cityid = c.city_id  WHERE  m.producttype=$3  ";
 
    if (cityid !=  null) {
        sql += " AND m.cityid = $4 ";
    }

    if (searchString !=  null && searchType != null) {
        sql += " AND m." + searchType + " ILIKE '%" + searchString + "%' ";
    }
    if (centerid !=  null) {
        sql += " AND m.centerid = $5 ";
    }
    if (ticketid !=  null) {
        sql += " AND m.ticketid = $6 ";
    }
    if (status !=  null) {
        sql += " AND m.medicinestatus = $7 ";
    }
 

    if (orderby != null && orderby != "") {
        sql += " order by $8 ";
        if (sortby != null && sortby != "") {
            sql += " $9";
        }
    }
    sql += " offset $1 limit $2 ";
     console.log('medicine list', sql, offset, limit,producttype,cityid,centerid,ticketid,status,orderby,sortby);
    db.any(sql, [offset, limit,producttype])
        .then(function (results) {
            res.status(200);
            res.send(
                /* {
                message: "users",
                id: results[0].currval
            } */
                results
            );
        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });

});


//::GETITEM
router.get('/:id', function (req, res) {
    var params = req.body;
    var id = req.params.id

    var sql = "SELECT * FROM medicine"
    sql += " WHERE medicineid=$1"
    db.any(sql, [id])
        .then(function (results) {
            var result = (results.length > 0) ? results[0] : {}
            res.status(200);
            res.send(result);
        })
        .catch(function (err) {
            // error;
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });
});

//::ADD
router.post('/', function (req, res) {
    var params = req.body;

    var ticketid = util.IsNull(params.ticketid);
    var userid = util.IsNull(params.userid);
    var medicinename = util.IsNull(params.medicinename);
    var medicinestatus = util.IsNull(params.medicinestatus);
    var medicinetype = util.IsNull(params.medicinetype);
    var expirydate = util.IsNull(params.expirydate);
    var companyname = util.IsNull(params.companyname);
    var totalquantity = util.IsNull(params.totalquantity);
    var prescriptionrequired = util.IsNull(params.prescriptionrequired);
    var addedby = util.IsNull(params.addedby);
    var centerid = util.IsNull(params.centerid);
    var rackno = util.IsNull(params.rackno);
    var availablequantity = util.IsNull(params.availablequantity);

    var sql = "INSERT INTO medicine(ticketid, userid, medicinename, medicinestatus, medicinetype, expirydate, companyname, totalquantity, prescriptionrequired,addedby,centerid,rackno,availablequantity)"
    sql += " VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,13)";
    console.log('medicine', sql);
    db.any(sql, [ticketid, userid, medicinename, medicinestatus, medicinetype, expirydate, companyname, totalquantity, prescriptionrequired, addedby, centerid, rackno, availablequantity])
        .then(function (results) {
            res.status(201);
            res.send({
                message: "created successfully"
            });
        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });
});

//::UPDATE
router.post('/:id', function (req, res) {
    var params = req.body;

    var id = req.params.id
    var ticketid = util.IsNull(params.ticketid);
    var userid = util.IsNull(params.userid);
    var medicinename = util.IsNull(params.medicinename);
    var medicinestatus = util.IsNull(params.medicinestatus);
    var medicinetype = util.IsNull(params.medicinetype);
    var expirydate = util.IsNull(params.expirydate);
    var companyname = util.IsNull(params.companyname);
    var totalquantity = util.IsNull(params.totalquantity);
    var prescriptionrequired = util.IsNull(params.prescriptionrequired);
    var centerid = util.IsNull(params.centerid);
    var rackno = util.IsNull(params.rackno);
    var availablequantity = util.IsNull(params.totalquantity); //availablequantity
    var updatedby = util.IsNull(params.updatedby);
    var batchno = util.IsNull(params.batchno);
    

    var sql = "UPDATE medicine SET ticketid =coalesce($2,ticketid),userid =coalesce($3,userid),medicinename=coalesce($4,medicinename),";
    sql += "medicinestatus=coalesce($5,medicinestatus),medicinetype=coalesce($6,medicinetype),expirydate=coalesce($7,expirydate),";
    sql += "companyname=coalesce($8,companyname),totalquantity=coalesce($9,totalquantity),";
    sql += "prescriptionrequired=coalesce($10,prescriptionrequired),centerid=coalesce($11,centerid),rackno=$12,availablequantity=coalesce($13,availablequantity),updatedby=coalesce($14,updatedby),batchno=coalesce($15,batchno)";
    sql += " WHERE medicineid=$1";

    console.log('Medicine update',sql);

    db.any(sql, [id, ticketid, userid, medicinename, medicinestatus, medicinetype, expirydate, companyname, totalquantity, prescriptionrequired, centerid, rackno, availablequantity, updatedby,batchno])
        .then(function (results) {
            res.status(200);
            res.send({
                status:201,
                message: "updated successfully",
                id: id
            });
        })
        .catch(function (err) {
            console.log('medicine update :: ', err);
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });
});

//::add medicine multiple
router.post('/insertmultiple/medicine', function (req, res) {
    var params = req.body; 
    var ticketid = util.IsNull(params.ticketid);
    var userid = util.IsNull(params.userid);
    var addedby = util.IsNull(params.addedby);
    var centerid = util.IsNull(params.centerid);
    var cityid = util.IsNull(params.cityid);
    var medicine =util.IsNull(params.medicinedata); 
    var medicineValue  ='';
    for (var i = 0; i < medicine.length; i++) {

       // console.log(medicine[i]['medicinename']);
        var medicinename = util.IsNull(medicine[i].medicinename);
        var medicinestatus = util.IsNull(medicine[i].medicinestatus);
        var medicinetype = util.IsNull(medicine[i].medicinetype);
        var expirydate = util.IsNull(medicine[i].expirydate);
        var companyname = util.IsNull(medicine[i].companyname);
        var totalquantity = util.IsNull(medicine[i].totalquantity);
        var prescriptionrequired = util.IsNull(medicine[i].prescriptionrequired);
        var rackno = util.IsNull(medicine[i].rackno);
        var availablequantity = util.IsNull(medicine[i].totalquantity);
        var batchno = util.IsNull(medicine[i].batchno);
        medicineValue   +="("+ticketid+","+ userid+",'"+ medicinename+"',"+ medicinestatus+","+ medicinetype+",'"+ expirydate+"','"+ companyname+"',"+ totalquantity+",'"+ prescriptionrequired+"',"+ addedby+","+ centerid+",'"+ rackno+"',"+ availablequantity+",'"+batchno+"','"+cityid+"'),";
    }
    var  medicineInputs = medicineValue.substring(0, medicineValue.length-1);  
    var sql = "INSERT INTO medicine(ticketid, userid, medicinename, medicinestatus, medicinetype, expirydate, companyname, totalquantity, prescriptionrequired,addedby,centerid,rackno,availablequantity,batchno,cityid)"
    sql += " VALUES " + medicineInputs;
    console.log('medicine', sql,medicineInputs);
     db.any(sql)
        .then(function (results) {
            res.status(201);
            res.send({
                status:201,
                message: "created successfully"
            });
        })
        .catch(function (err) {
            console.log('Insert multiple Error',err);
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });  
    
});




//::add medicine multiple
router.post('/add/otherproduct/', function (req, res) {
    var params = req.body; 
    var ticketid = util.IsNull(params.ticketid);
    var userid = util.IsNull(params.userid);
    var addedby = util.IsNull(params.addedby);
    var centerid = util.IsNull(params.centerid);
    var cityid = util.IsNull(params.cityid);
    var medicine =util.IsNull(params.medicinedata); 
    var medicinename  ='Other Items - Toys';
    var medicinestatus =1;
    var medicinetype =0;
    var availablequantity =1; 
    var totalquantity =1;
    var producttype=2; 
    var otheritemdetails= util.IsNull(params.donarcomment);

    var sql = "INSERT INTO medicine(ticketid, userid, medicinename, medicinestatus, medicinetype, availablequantity, totalquantity, addedon, addedby, centerid,cityid, producttype, otheritemdetails)  VALUES($1,$2,$3,$4,$5,$6,$7,now(),$8,$9,$10,$11,$12) ";
    console.log('Insert other items', sql );

     db.any(sql,[ticketid, userid, medicinename, medicinestatus, medicinetype, availablequantity, totalquantity,addedby, centerid,cityid, producttype, otheritemdetails])
        .then(function (results) {
            res.status(201);
            res.send({
                status:201,
                message: "created successfully"
            });
        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });  
    
});
//medicine Name suggestion

router.get('/medicine/suggestion', function (req, res) {
    var params = req.body;
    var medicinename = req.query.medicinename
    var limit = util.IsNull(req.query.limit,50);
    var sql = "SELECT * FROM medicinemaster WHERE medicinename iLIKE '"+medicinename+"%'  offset 0 limit $2 ";
    console.log('med sugg ::',sql);
    db.any(sql, [medicinename,limit])
        .then(function (results) {
            var result = (results.length > 0) ? results : {}
            res.status(200);
            res.send(result);
        })
        .catch(function (err) {
           console.log('medicinesuggestion :: ',err);
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });
});



module.exports = router;