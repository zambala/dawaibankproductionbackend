var express = require('express');
var router = express.Router();
var db = require('./dbconnections_controller');
var util = require('./../config/helper');
var path = require('path');
var fs = require('fs');
var xlsx = require('xlsx');
var fileName = 'test1.xlsx';
var filePath = path.join(__dirname, './../' + fileName);

router.get('/', function (req, res, next) { 
    return false;
    fs.exists(filePath, function (exists) {
        if (exists) {
            console.log('file exits'); 
            workbook = xlsx.readFile(filePath); 
            var mainSheet1 = workbook.Sheets['product complete 7393'];
            var companyName = '';
            var medicineName = '';
            var dataObject = [];
            for (var cell in mainSheet1) {
                var col = cell.match(/[^\d]+|\d+/g)[0];
                var cellNo = cell.match(/[^\d]+|\d+/g)[1];

                if (cellNo > 1) {
                     //  console.log(cell, col, cellNo, mainSheet1[cell].v); 
                    if (col == 'A') medicineName = mainSheet1[cell].v;
                    else if (col == 'B') {
                        companyName = mainSheet1[cell].v;
                        dataObject.push({'medicinename':medicineName,'companyname':companyName});
                        console.log('Check medicine Name :: ', medicineName, ' :: ',companyName); 

                    }
                }
            }
          //  console.log(dataObject);
          var count =0;
            dataObject.forEach(function(element) {
                var tempMedicineName = element.medicinename;
                var tempCompanyName = element.companyname;
                var sqlCheckMedicinename = "SELECT * FROM medicinemaster WHERE medicinename=$1";
              //  console.log(sqlCheckMedicinename);
                db.any(sqlCheckMedicinename, [tempMedicineName])
                    .then(function (results) {

                        if (results.length == 0) {
                            count++;
                            var sql = "INSERT INTO medicinemaster(masterid,medicinename, companyname) "
                            sql += " VALUES(nextval('masterid_seq'),$1,$2) ";
                          //  console.log('Insert ', tempMedicineName, tempCompanyName);
                            db.any(sql, [tempMedicineName, tempCompanyName])
                                .then(function (insertresult) {
                                    // console.log('inserted master medicien');
                                })
                                .catch(function (err) {
                                    console.log('ERROR USER CREATION', err);
                                    var code = err.code == 23505 ? 409 : 400
                                    res.status(code);
                                    res.send(err);
                                });

                        } else {
                            console.log('Name already exists', medicineName);
                        }
                    })
                    .catch(function (err) {
                        console.log('Medicine Dump error ::', err);
                        var code = err.code == 23505 ? 409 : 400
                        res.status(code);
                        res.send(err);
                    }); 
              });
        }

        

    });

});



module.exports = router;