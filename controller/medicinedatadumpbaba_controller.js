var express = require('express');
var router = express.Router();
var db = require('./dbconnections_controller');
var util = require('./../config/helper');
var path = require('path');
var fs = require('fs');
var xlsx = require('xlsx');
var fileName = '249444.xlsx';
var filePath = path.join(__dirname, './../' + fileName);

router.get('/', function (req, res, next) { 
    console.log('medicine xl sheet data dump updated..');
   return false;
    fs.exists(filePath, function (exists) {
        if (exists) {
            console.log('file exits'); 
            workbook = xlsx.readFile(filePath); 
            var mainSheet1 = workbook.Sheets['Input sheet'];
            
            var medicineName = ''; var MedicineType=''; var Quantity='';
            var companyName = '';  var ExpiryDate=''; var RackNo=''; var BatchNo='';
            var dataObject = [];
            for (var cell in mainSheet1) {
                var col = cell.match(/[^\d]+|\d+/g)[0];
                var cellNo = cell.match(/[^\d]+|\d+/g)[1];
               // console.log(cellNo);
                if (cellNo > 1) {
                     //  console.log(cell, col, cellNo, mainSheet1[cell].v); 
                    if (col == 'A') medicineName = mainSheet1[cell].v;
                    if (col == 'B') MedicineType = mainSheet1[cell].v;
                    if (col == 'C') Quantity = mainSheet1[cell].v;
                    if (col == 'D') companyName = mainSheet1[cell].v;
                    if (col == 'E') ExpiryDate = mainSheet1[cell].w;
                    if (col == 'F') RackNo = mainSheet1[cell].v; 
                    else if (col == 'G') {
                      //  console.log('*********** Insert Data ***************'); 
                        BatchNo = mainSheet1[cell].v;
                        dataObject.push({'medicineName':medicineName,'MedicineType':MedicineType,'Quantity':Quantity,'companyname':companyName,'ExpiryDate':ExpiryDate,'RackNo':RackNo,'BatchNo':BatchNo});
                      //  console.log('medicineName : ',medicineName,'MedicineType ',MedicineType,'Quantity :',Quantity,'CompanyName :',companyName,'ExpiryDate : ',ExpiryDate,RackNo,BatchNo); 

                    }
                }
            }

            dataObject.forEach(function(element) {


               // var sql = "INSERT INTO medicinemaster(masterid,medicinename, companyname) "
               // sql += " VALUES(nextval('masterid_seq'),$1,$2) ";
              //  var medicineName = element.medicineName;
              var medicinetypeObj = {"TABLET":1, "CAPSULE":4, "POWDER":5, "SYRUP":6, "EYE DROP":7, "OINTMENT GEL":8, "INJECTION":3, "INHALER":9, "RESPULES":10};
                var MedicineType_inputformat = element.MedicineType;
               // console.log(MedicineType,medicinetypeObj[MedicineType_inputformat] );
     


                  
                var ticketid ='';               
                var medicinename = element.medicineName;
                var medicinestatus = 1; 
                var medicinetype = medicinetypeObj[MedicineType_inputformat]; 
                var expirydate = element.ExpiryDate; 
                var companyname = element.companyname; 
                var prescriptionrequired = 0;
                var deliveryto = 0; 
                var deliverby = 0; 
                var comment = "";                
                var rackno = ""; 
                var availablequantity = element.Quantity; 
                var totalquantity = element.Quantity;               
               
                var batchno = element.BatchNo;                
                var otheritemdetails= ""; 
                var cityid = 278; 
                var producttype = 1; 
                var addedby = 10; 
                var updatedby = 10; 
                var centerid = 23; 
                var userid =1;

                

                var sql = " INSERT INTO medicine (userid, medicinename, medicinestatus, medicinetype, expirydate, companyname, prescriptionrequired, deliveryto,  deliverby, comment, centerid, rackno, availablequantity, totalquantity, addedby, updatedby, batchno, cityid, producttype, otheritemdetails,addedon) ";
                sql += " VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,now())"; 
              // console.log(sql);
               console.log( userid, medicinename, medicinestatus, medicinetype, expirydate, companyname, prescriptionrequired, deliveryto,  deliverby, comment, centerid, rackno, availablequantity, totalquantity, addedby, updatedby, batchno, cityid, producttype, otheritemdetails);
               db.any(sql, [userid, medicinename, medicinestatus, medicinetype, expirydate, companyname, prescriptionrequired, deliveryto, deliverby, comment, centerid, rackno, availablequantity, totalquantity, addedby, updatedby, batchno, cityid, producttype, otheritemdetails]).then(function (results) {
                        console.log('inserted master medicien');
                    })
                    .catch(function (err) {
                        console.log('ERROR USER CREATION', err);
                        var code = err.code == 23505 ? 409 : 400
                        res.status(code);
                        res.send(err);
                    });  

            });
          
        }

        

    });

});



module.exports = router;